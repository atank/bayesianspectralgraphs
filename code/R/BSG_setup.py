from __future__ import division

import os
import numpy as np

from itertools import product



jfile = "BSG.job"

T = [500,1000,2500,5000,7500,10000]
Tm = [25,50, 100, 500, 1000,1500, 2000]
method = [1, 2, 3]
Nseries = [25, 50, 100, 150, 200, 250, 300]
ex = range(1,101,1)
p = 20
pp = [10,15,20,25]
seed = 345378273
###edit basecmd, as this will depend on which computer you run everything on
#storedatacmd = "/Users/alextank/Documents/BayesianSpectralGraphs/code/cpp/spectral_fincsgraphs/"

storedatacmd = "/data/spectral/"
try:
    os.makedirs(storedatacmd)
except OSError:
    pass

try:
    os.makedirs("%slogs" % storedatacmd)
except OSError:
    pass



try:
    os.makedirs(storedatacmd + "G_recovery")
except OSError:
    pass

basecmd = "./runexper.sh"
with open(jfile, 'w') as f:
    #loop over configurations for single time series
    par_prod = product(T,method,ex)
    for par in par_prod:
        t,m,e = par
        cmd = basecmd + " " + str(p) + " " + str(t) + " " + "1" + " " + str(m) + " " + str(e) + " " + str(seed) + " " + storedatacmd
        outfile = "%slogs/var_" % storedatacmd + str(p) + "_" + str(t) + "_" + "1" + "_" + str(m) + "_" + str(e) + "_" + str(seed) + ".log"
        cmd = cmd + " &> " + outfile
        f.write(cmd + "\n")

    # #loop over configurations for multiple time series
    par_prod = product(Tm,Nseries,ex)
    for par in par_prod:
         t,n,e = par
         cmd = basecmd + " " + str(p) + " " + str(t) + " " + str(n) + " " + "1" + " " + str(e) + " " + str(seed) + " " + storedatacmd
         outfile = "%slogs/var_" % storedatacmd + str(p) + "_" + str(t) + "_" + str(n) + "_" + "1" + "_" + str(e) + "_" + str(seed) + ".log"
         cmd = cmd + " &> " + outfile
         f.write(cmd + "\n")

    par_prod = product(pp,ex)
    for par in par_prod:
        pp,e = par
        cmd = basecmd + " " + str(pp) + " " + str(1000) + " " + str(100) + " " + "1" + " " + str(e) + " " + str(seed) + " " + storedatacmd
        outfile = "%slogs/var_" % storedatacmd + str(pp) + "_" + str(1000) + "_" + "100" + "_" + "1"  + "_" + str(e) + "_" + str(seed) + ".log"
        cmd = cmd + " &> " + outfile
        f.write(cmd + "\n")



os.chmod(jfile, 0755)




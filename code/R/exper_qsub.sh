#!/bin/bash

# Submit this job like `qsub -t 1-MAX -V -b yes -cwd ./exper_qsub.sh`
# You can get MAX with something like `wc -l BSG.job`.

CMD=$(awk "NR==$SGE_TASK_ID" BSG.job)
eval $CMD

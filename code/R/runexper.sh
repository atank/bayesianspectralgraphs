#!/bin/bash
#run R script to generate the data for the parameter configurations
Rscript gendata.R $1 $2 $3 $4 $5 $6 $7

###cd to the folder to do inference
cd ../cpp/spectral_fincsgraphs

inputfile="$7/var_"
inputfile+="$1_$5_$4_$2_$3"
infile="$inputfile.infile"
echo $infile
start=`date +%s`
./fincs < $infile
end=`date +%s`
runtime=$((end-start))
cd ../../R

mkdir -p $7/G_recovery
###compute the false positive and false negative rate
Rscript ROC.R $1 $2 $3 $4 $5 $6 $runtime $7

cd $7
rm $inputfile.*

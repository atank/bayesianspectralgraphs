#include "fincs.h"
#define TOLORDERMAG 10

// Global declarations and initialization of static member variables
Matrix Temp;
Matrix GGM::X = Temp;
ushort GGM::nObs = 0;
ushort GGM::nNodes = 0;
ushort GGM::MaxEdges = 0;
double GGM::g = 0.0;
double GGM::tau = 0.0;
double GGM::delta = 0.0;
double GGM::pHat = 0.5;
RNG GGM::Random;
SymmetricMatrix GGM::EdgeWeights;
SymmetricMatrix GGM::Inclusion;
double GGM::adjust = 0.05;

int main(void)
{
  // Declare some variables we need
  int verbose = 100;
  double** Data=NULL;
  double CurrentScore = 1.0, SumScores = 1.0, WorstScore = 1.0, BestScore = 1.0, bestlogscore = 0.0;
  char DataFile[512];
  FILE* in;
  int t, iterations;
  int coinflip, revisit;
  ushort LikeMethod, PriorMethod;
  GGMMap SavedModels;
  int TopModels;
  set<double, tolcomp> DiscoveredScores;
  pair<set<double,tolcomp>::iterator, bool> discovered;

  RNG Generator(rand());
  GGM::Random = Generator;

  // Scan values of variables from infile

  scanf("%s", DataFile);
  cin>> (GGM::nObs);           	// Number of rows of X 
  cin>> (GGM::nNodes);	       	// Number of columns of X
  cin>>iterations;		// Number of iterations
  cin>>coinflip;		// How often to make RMTP move
  cin>>revisit;			// How often to revisit a good model
  cin>>LikeMethod;		// Method to compute the marginal likelihoods
  cin>>PriorMethod;             // Method to compute prior model probabilities
  cin>> TopModels;  	        // Number of top models to keep in RAM
  cin>> (GGM::g);
  cin>>GGM::delta;
  cin>>GGM::tau;
  cin>>GGM::pHat;               // common prior inclusion probability
  cin>>verbose;                 // How often to print stuff


  // Initialize other static class variables
  GGM::MaxEdges = GGM::nNodes*(GGM::nNodes-1)/2;
  GGM::EdgeWeights = 0.0;
  GGM::Inclusion = 0.0;
  SavedModels.TopModels  = TopModels;

  //read in the data
  Data = new double*[GGM::nObs];
  ushort i, j;
  double s;
  for(i=0;i<GGM::nObs;i++) Data[i] = new double[GGM::nNodes];
  in = fopen(DataFile,"r");
  if(NULL==in) {printf("Cannot open data file %s.\n",DataFile); exit(1);}
  for(i=0;i<GGM::nObs;i++)
  {
    for(j=0;j<GGM::nNodes;j++)
    {
	fscanf(in,"%lf",&s);
        Data[i][j] = s;
    }
  }
  fclose(in);

  Matrix XTemp(GGM::nObs, GGM::nNodes);
  for(i=0; i<GGM::nObs; i++)
  {
    XTemp.Row(i+1) << Data[i];
  }
  GGM::X = XTemp;

// Now clean up data array
  for(i=0;i<GGM::nObs;i++)
  {
    delete[] Data[i]; 
    Data[i] = NULL;
  }
  delete[] Data;
  Data = NULL;

  cout << "Loaded data.\n";
  cout << "Nodes = " << setprecision(0) << GGM::nNodes << endl;
  cout << "Observations = " << setprecision(0) << GGM::nObs << endl;
  cout << "g = " << setprecision(0) << GGM::g << endl;
  cout << "delta = " << setprecision(0) << GGM::delta << endl;

  RNG Random;

// Initialize the model and append the first model to the saved models list
  DecomposableGraph CurrentGraph(GGM::nNodes);
  EdgeList L;
  L = CurrentGraph.GenerateStartingGraph(3.0);

  CurrentGraph.InitializeGraphFBF(L, true);
  cout << "Initial log marginal likelihood: " << setprecision(10) << CurrentGraph.logscore << endl;
  SavedModels.Correction = CurrentGraph.logscore;
  CurrentScore = exp(CurrentGraph.logscore - SavedModels.Correction);
  bestlogscore = CurrentGraph.logscore;
  (SavedModels.ModelMap).insert(pair<double, DecomposableGraph>(CurrentScore, CurrentGraph));
  GGM::EdgeWeights = CurrentGraph.GetIncidence();
  GGM::Inclusion = CurrentGraph.GetIncidence();
  cout << "Initial graph:: " << endl;
  cout << setprecision(0) << CurrentGraph.GetIncidence();

  double resamplekey;
  bool foundnew;
  map< double, DecomposableGraph >::iterator map_it;
  // Main loop begins here

//   DecomposableGraph BurnGraph(GGM::nNodes);
//   for(t=1; t<=100000; t++)
//     {
//       DecomposableGraph NewGraph(GGM::nNodes);
//       NewGraph = CurrentGraph;
//       foundnew = NewGraph.MetropolisMoveFBF();
//       if(foundnew && NewGraph.logscore - CurrentGraph.logscore > log(Random.uniform(0.0,1.0)))
// 	{
// 	  CurrentGraph = NewGraph;
// 	}
//       if(CurrentGraph.logscore > BurnGraph.logscore)
// 	{
// 	  BurnGraph = CurrentGraph;
// 	}
//       if(t % verbose == 0)
// 	{
// 	  cout << "Burn-in: " << t << endl;
// 	  cout << "Current best log score: " << setprecision(10) << BurnGraph.logscore << endl;
// 	}
//     }
//  CurrentGraph = BurnGraph;

  cout << "Initial log marginal likelihood: " << setprecision(10) << CurrentGraph.logscore << endl;
  SavedModels.Correction = CurrentGraph.logscore;
  CurrentScore = exp(CurrentGraph.logscore - SavedModels.Correction);
  bestlogscore = CurrentGraph.logscore;

  std::clock_t start = std::clock();
  for(t = 1; t <= iterations; t++)
    {

      GGM::adjust = (0.1)*(double)(iterations - t)/(double)iterations;
      if(t % verbose == 0)
	{
	  cout << "Iteration " << t << endl;
	  cout << "Current best log score: " << setprecision(10) << bestlogscore << endl;
// 	  cout << "Current best graph: " << endl;
// 	  cout << setprecision(0) << (SavedModels.ModelMap).rbegin()->second.GetIncidence() << endl;
	}

      if(t % coinflip == 0)
	// Flip and triangulate
	{
	  CurrentGraph.RandomMedianGraph(1.0/(double)GGM::MaxEdges);
	  cout << setprecision(0) << CurrentGraph.GetIncidence();
	  for(int j = 0; j< (int)GGM::MaxEdges; j++)
	    {

	      DecomposableGraph NewGraph(GGM::nNodes);
	      NewGraph = CurrentGraph;
	      foundnew = NewGraph.MetropolisMoveFBF();
	      if(foundnew && NewGraph.logscore - CurrentGraph.logscore > log(Random.uniform(0.0,1.0)))
		{
		  CurrentGraph = NewGraph;
		}
	    }
	}

      else if(t % revisit == 0)
	// resample an old good model
	// Then do a local move
	{
	  resamplekey = Random.uniform(0.0,BestScore);
	  if(Random.uniform(0.0,1.0) > 0.9) resamplekey = BestScore;
	  map_it = (SavedModels.ModelMap).lower_bound(resamplekey);
	  CurrentGraph = map_it->second;
	  foundnew = CurrentGraph.FINCSLocalMoveFBF();
	}

      else
	// Local move only
	{
	  foundnew = CurrentGraph.FINCSLocalMoveFBF();
	}
      discovered.second = false;
      if(foundnew) discovered = DiscoveredScores.insert(CurrentGraph.logscore);
      if(discovered.second)
	// We have a new model on our hands
	// Use it to update inclusion probabilities
	// Check if it belongs on the "Best-Of" list for resampling
	{
	  if(CurrentGraph.logscore - bestlogscore > TOLORDERMAG)
	    // We could have some numerical problems since the new model
	    // is WAY WAY better than the old ones
	    // So just start the accounting over!
	    {
	      bestlogscore = CurrentGraph.logscore;
	      GGM::EdgeWeights = CurrentGraph.GetIncidence();
	      SavedModels.Correction = CurrentGraph.logscore;
	      (SavedModels.ModelMap).clear();
	      CurrentScore = 1.0;
	      (SavedModels.ModelMap).insert(pair<double, DecomposableGraph>(CurrentScore, CurrentGraph));
	      WorstScore = 1.0;
	      SumScores = 1.0;
	      BestScore = 1.0;
	      GGM::Inclusion = GGM::EdgeWeights;
	    }
// 	  else if(CurrentGraph.logscore - SavedModels.Correction > 10)
// 	    // Renormalize
// 	    {
// 	      CurrentScore = exp(CurrentGraph.logscore - SavedModels.Correction);
// 	      SumScores += CurrentScore;
// 	      GGM::EdgeWeights += ( max(0.0,CurrentScore) ) * CurrentGraph.GetIncidence();
// 	    }
	  else
	    {
	      CurrentScore = exp(CurrentGraph.logscore - SavedModels.Correction);
	      SumScores += CurrentScore;
	      GGM::EdgeWeights += ( max(0.0,CurrentScore) ) * CurrentGraph.GetIncidence();
	      GGM::Inclusion = GGM::EdgeWeights/SumScores;
	      if(CurrentScore > BestScore)
		{
		  // We have a new best score
		  BestScore = CurrentScore;
		  bestlogscore = CurrentGraph.logscore;
		  (SavedModels.ModelMap).insert(pair<double, DecomposableGraph>(CurrentScore, CurrentGraph));
		}
	      else if( (SavedModels.ModelMap).size() < SavedModels.TopModels || CurrentScore > WorstScore)
		// Better than the worst model on the resampling list
		// Or we simply haven't saved enough models yet
		{
		  (SavedModels.ModelMap).insert(pair<double, DecomposableGraph>(CurrentScore, CurrentGraph));
		}
	      if( (SavedModels.ModelMap).size() > SavedModels.TopModels)
		// We've got more than we want to save in the resample list
		// Push the worst one off
		{
		  (SavedModels.ModelMap).erase( (SavedModels.ModelMap).begin() );
		}
	    }
	}
    }

  // If we didn't save TopModels models, reset the number so we don't segfault
  SavedModels.TopModels = (SavedModels.ModelMap).size();
  std::cout << "Saving " << SavedModels.TopModels << " models: " << std::endl;

  double runtime = ((std::clock() - start) / static_cast<double>(CLOCKS_PER_SEC));
  std::cout << "Runtime: " << setprecision(5) << runtime << " seconds\n";

  ofstream outfile;

  time_t curr;
  time(&curr);

  outfile.open("FINCSproperties.txt");
  outfile << "Time of Run: " << ctime(&curr) << "\n";
  outfile << "LikeMethod: " << LikeMethod << "\n";
  outfile << "PriorMethod: " << PriorMethod << "\n";
  outfile << "Iterations: " << iterations << "\n";
  outfile << "Models Visited: " << DiscoveredScores.size() << "\n";
  outfile << "Run time: " << ((std::clock() - start)/(double)CLOCKS_PER_SEC) << endl;
  outfile.close();

  outfile.open ("FINCSinclusion.txt");
  outfile << setw(5) << setprecision(3) << GGM::Inclusion;
  outfile.close();

  std::set<double,tolcomp>::reverse_iterator rit;
  outfile.open ("FINCSlogscores.txt");
  rit = DiscoveredScores.rbegin();
  for(i=0;i<min(1000,(int)DiscoveredScores.size());i++)
    {
      outfile << setprecision(10) << *rit << endl;
      rit++;
    }
  outfile.close();

  outfile.open ("FINCSbestgraphs.txt");
  map<double, DecomposableGraph>::reverse_iterator mit = SavedModels.ModelMap.rbegin();
  int upper = min(1000,(int)SavedModels.TopModels);
  for(ushort i = 0; i < upper; i++)
  {
      outfile << setprecision(0) << mit->second.GetIncidence() << endl;
      mit++;
  }
  outfile.close();

  return 0;
}

#include "graph.h"
#include "rng.h"

extern RNG Random;
extern SymmetricMatrix EdgeWeights;

JunctionTreeNode::~JunctionTreeNode(void)
{
  Neighbors.clear();
}


DecomposableGraph::DecomposableGraph(ushort n)
{
  NodeSet EmptySet;
  vector< NodeSet > temp(n, EmptySet);
  this->AdjacencyVector = temp;
  this->logscore = 0.;
  this->nEdges = 0;
  this->nNodes = n;
}

DecomposableGraph::DecomposableGraph(const DecomposableGraph& G)
{
  AdjacencyVector = G.AdjacencyVector;
  logscore = G.logscore;
  nEdges = G.nEdges;
  nNodes = G.nNodes;
  EdgeWeights = G.EdgeWeights;
  Inclusion = G.Inclusion;
  list<JunctionTreeNode>::const_iterator jt_i, jt_l, jt_m;
  list<JunctionTreeNode>::iterator jt_j, jt_k;
  JunctionTreeNode JTN;
  ushort dist = 0;
  list<list<JunctionTreeNode>::iterator>::const_iterator jtiter_j;
  for(jt_i = G.JunctionForest.begin(); jt_i != G.JunctionForest.end(); jt_i++)
    {
      JTN.C = jt_i->C;
      JunctionForest.push_back(JTN);
    }

  jt_k = JunctionForest.begin();
  for(jt_i = G.JunctionForest.begin(); jt_i != G.JunctionForest.end(); jt_i++)
    {
      for(jtiter_j = jt_i->Neighbors.begin(); jtiter_j != jt_i->Neighbors.end(); jtiter_j++)
	{
	  dist = 0;
	  if((*jtiter_j)->SearchColor == true) {continue;}
	  jt_j = JunctionForest.begin();
	  jt_l = *jtiter_j;
	  jt_m = G.JunctionForest.begin();
	  while(jt_m != jt_l) {dist++; jt_m++;}
	  while(dist != 0) {jt_j++; dist--;}
	  jt_j->Neighbors.push_back(jt_k);
	}
      jt_k->SearchColor = true;
      jt_k++;
    }
  for(jt_j = JunctionForest.begin(); jt_j != JunctionForest.end(); jt_j++)
    {
      jt_j->SearchColor = false;
    }
}

DecomposableGraph& DecomposableGraph::operator=(const DecomposableGraph& G)
{
  if (this != &G)
    // Ensure no errors in self-assignment
    {
      JunctionForest.clear();
      AdjacencyVector = G.AdjacencyVector;
      logscore = G.logscore;
      nEdges = G.nEdges;
      nNodes = G.nNodes;
      EdgeWeights = G.EdgeWeights;
      Inclusion = G.Inclusion;
      std::list<JunctionTreeNode>::const_iterator jt_i, jt_l, jt_m;
      list<JunctionTreeNode>::iterator jt_j, jt_k;
      JunctionTreeNode JTN;
      ushort dist = 0;
      list<list<JunctionTreeNode>::iterator>::const_iterator jtiter_j;
      for(jt_i = (G.JunctionForest).begin(); jt_i != (G.JunctionForest).end(); jt_i++)
	{
	  JTN.C = jt_i->C;
	  JunctionForest.push_back(JTN);
	}

      jt_k = JunctionForest.begin();
      for(jt_i = (G.JunctionForest).begin(); jt_i != (G.JunctionForest).end(); jt_i++)
	{
	  for(jtiter_j = jt_i->Neighbors.begin(); jtiter_j != jt_i->Neighbors.end(); jtiter_j++)
	    {
	      dist = 0;
	      if((*jtiter_j)->SearchColor == true) {continue;}
	      jt_j = JunctionForest.begin();
	      jt_l = *jtiter_j;
	      jt_m = G.JunctionForest.begin();
	      while(jt_m != jt_l) {dist++; jt_m++;}
	      while(dist != 0) {jt_j++; dist--;}
	      jt_j->Neighbors.push_back(jt_k);
	    }
	  jt_k->SearchColor = true;
	  jt_k++;
	}
      for(jt_j = JunctionForest.begin(); jt_j != JunctionForest.end(); jt_j++)
	{
	  jt_j->SearchColor = false;
	}
    }
  return *this;
}


void DecomposableGraph::PrintGraph(void)
{
  cout << "Adjacency list:" << setprecision(0) << endl;
  ushort i;
  for(i = 1; i <= this->nNodes; i++)
    {
      cout << i << ": ";
      NodeSetPrint(this->AdjacencyVector.at(i-1));
      cout << endl;
    }
}

void DecomposableGraph::PrintJunctionTree(void)
{
  cout << "Junction Tree:" << setprecision(0) << endl;
  list<JunctionTreeNode>::iterator jt_i;
  list< list<JunctionTreeNode>::iterator>::iterator neighbor_i;
  for(jt_i = this->JunctionForest.begin(); jt_i != this->JunctionForest.end(); jt_i++)
    {
      NodeSetPrint(jt_i->C); cout << ": ";
      for(neighbor_i = jt_i->Neighbors.begin(); neighbor_i != jt_i->Neighbors.end(); neighbor_i++)
	{
	  NodeSetPrint((*neighbor_i)->C); cout << ", ";
	}
      cout << endl;
    }
}

bool DecomposableGraph::AreNeighbors(ushort a, ushort b)
{
  bool neighbors = false;
  set<ushort>::iterator set_it = (this->AdjacencyVector.at(a-1)).find(b);
  if(set_it != (this->AdjacencyVector.at(a-1)).end()) {neighbors = true;}
  return neighbors;
}



EdgeList GGM::GenerateStartingGraph(double threshold)
{
  int i, j;
  double s;
  EdgeList L;
  // Now do triangular regression to compute an initial graph
  Matrix XTemp;
  ColumnVector Y;
  ColumnVector BetaHat;
  Matrix Info;
  Matrix M;
  double z;
  this->nEdges = 0;
  for(i = 1; i < GGM::nNodes; i++)
    {
      Y = (GGM::X).Column(i);
      XTemp = (GGM::X).Columns(i+1, GGM::nNodes);
      Info = (XTemp.t() * XTemp).i();
      M = XTemp * Info * XTemp.t();
      s = sqrt( (Y.SumSquare() - (Y.t() * M * Y).AsScalar())/(GGM::nObs - GGM::nNodes) );
      BetaHat = Info * XTemp.t() * Y;
      for(j=1; j <= GGM::nNodes - i; j++)
	{
	  z = s * sqrt(Info(j,j));
	  if(abs(BetaHat(j)) >= threshold*z)
	    {
	      L.push_back( pair<ushort, ushort> (i, i+j) );
	      this->nEdges++;
	    }
	}
    }
  return(L);
}

void DecomposableGraph::RandomMedianGraph(double tol)
{
  int i, j;
  EdgeList L;
  // Now do triangular regression to compute an initial graph
  this->nEdges = 0;
  for(i = 1; i < GGM::nNodes; i++)
    {
      for(j=1; j <= GGM::nNodes - i; j++)
	{
	  if(Random.uniform(0.0-tol,1.0+tol) < GGM::Inclusion(i,j) )
	    {
	      L.push_back( pair<ushort, ushort> (i, i+j) );
	      this->nEdges++;
	    }
	}
    }
  this->InitializeGraphFBF(L, true);
}


NodeSet DecomposableGraph::Neighbors(ushort n)
{
  return ((this->AdjacencyVector).at(n-1));
}

ushort DecomposableGraph::Size(void)
{
  return AdjacencyVector.size();
}

list<JunctionTreeNode>::iterator DecomposableGraph::FindCliqueContaining(NodeSet K)
// Returns an iterator to the first clique in the junction tree containing NodeSet K
{
  list<JunctionTreeNode>::iterator it = this->JunctionForest.begin();
  bool found = false;
  while(it != this->JunctionForest.end() && found == false)
    {
      found = IsSubset(K, it->C);
      if(!found) it++;
    }
  return(it);
}


pair< bool, pair< list<JunctionTreeNode>::iterator, list<JunctionTreeNode>::iterator > > DecomposableGraph::FindSeparator(NodeSet K)
// Returns a pair
// First element is a boolean indicating whether the set is a separator on the junction tree
// second element is iterator to a node in the junction forest having separator K
{
  list<JunctionTreeNode>::iterator jt_i = this->JunctionForest.begin();
  list< list<JunctionTreeNode>::iterator>::iterator jt_j;
  bool found=false;
  bool subset = false;
  NodeSet Int;
  while(jt_i != this->JunctionForest.end() && found == false)
    {
      subset = IsSubset(K,jt_i->C);
      if(subset)
	{
	  jt_j = jt_i->Neighbors.begin();
	  while(jt_j != jt_i->Neighbors.end() && found == false)
	    {
	      Int = Intersection(jt_i->C, (*jt_j)->C);
	      if(K == Int) {found = true;}
	      else {jt_j++;}
	    }
	}
      if(!found) {jt_i++;} //Since if we found it we don't want to return wrong node
    }
  pair< list<JunctionTreeNode>::iterator, list<JunctionTreeNode>::iterator > return_pair(jt_i,*jt_j);
  return (pair< bool, pair< list<JunctionTreeNode>::iterator, list<JunctionTreeNode>::iterator > >(found, return_pair));
}



bool DecomposableGraph::CanAddEdge(ushort a, ushort b)
{
  cout << "Trying to add (" << a << ", " << b << ")" << endl;
  bool samecc, canadd = false;
  set<ushort>::iterator set_it = (this->AdjacencyVector.at(a-1)).find(b);
  if(set_it != (this->AdjacencyVector.at(a-1)).end()) {canadd = false;}
  else
    {
      NodeSet Ca, Cb; Ca.insert(a); Cb.insert(b);
      list<JunctionTreeNode>::iterator jt_a, jt_b;
      jt_a = FindCliqueContaining(Ca);
      jt_b = FindCliqueContaining(Cb);
      pair< bool, list< list<JunctionTreeNode>::iterator> > return_pair;
      return_pair = DFSJunctionTree(jt_a, jt_b);
      samecc = return_pair.first;
      if(!samecc)
	{
	  canadd = true;
	}
      else
	{
	  NodeSet K; K = Intersection(Neighbors(a), Neighbors(b));
	  NodeSet Ca = K, Cb = K; Ca.insert(a); Cb.insert(b);
	  jt_a = FindCliqueContaining(Ca);
	  jt_b = FindCliqueContaining(Cb);
	  pair< bool, list< list<JunctionTreeNode>::iterator> > return_pair;
	  return_pair = DFSJunctionTree(jt_a, jt_b);
	  if(K.empty()) {canadd = false;}
	  else
	    {
	      list< list<JunctionTreeNode>::iterator>::iterator list_it, list_jt;
	      list_it = (return_pair.second).begin();
	      while(list_it != (return_pair.second).end() && !canadd)
		{
		  list_jt = list_it; list_jt++;
		  if( Intersection( (*list_it)->C, (*list_jt)->C ) == K)
		    {
		      canadd = true;
		    }
		  else {list_it++;}
		}
	    }
	}
    }
  return canadd;
}

bool DecomposableGraph::CanRemoveEdge(ushort a, ushort b)
{
  //cout << "Trying to delete (" << a << ", " << b << ")" << endl;
  bool candelete = true;
  set<ushort>::iterator set_it = (this->AdjacencyVector.at(a-1)).find(b);
  if(set_it == (this->AdjacencyVector.at(a-1)).end()) {candelete = false;}
  else
    {
      NodeSet K; K.insert(a), K.insert(b);
      NodeSet Sep;
      list<JunctionTreeNode>::iterator jt_i = this->JunctionForest.begin();
      list< list<JunctionTreeNode>::iterator>::iterator jt_j;
      while(jt_i != this->JunctionForest.end() && candelete)
	{
	  jt_j = jt_i->Neighbors.begin();
	  while(jt_j != jt_i->Neighbors.end() && candelete)
	    {
	      Sep = Intersection(jt_i->C, (*jt_j)->C);
	      if (K == Intersection(K,Sep)) {candelete = false;}
	      else {jt_j++;}
	    }
	  jt_i++;
	}
    }
  return candelete;
}

bool DecomposableGraph::AddEdgeFBF(ushort a, ushort b)
{
  // First check to see if we can add
  //cout << "Trying to add edge (" << a << ", " << b << ")" << endl;
  list<JunctionTreeNode>::iterator jt_a, jt_b, jt_i;
  list< list<JunctionTreeNode>::iterator >::iterator list_it, list_jt;
  NodeSet Sep = Intersection(Neighbors(a), Neighbors(b));
  NodeSet Cq, Cq1, Cq2, Sq2;
  Sq2 = Sep;
  Cq1 = Sep; Cq1.insert(a);
  Cq2 = Sep; Cq2.insert(b);
  Cq = Cq2; Cq.insert(a);
  NodeSet Ca, Cb;
  pair< bool, list< list<JunctionTreeNode>::iterator> > return_pair;
  bool samecc, canadd = false, containssep=false, found = false, subfound = false;
  set<ushort>::iterator set_it = (this->AdjacencyVector.at(a-1)).find(b);
  if(set_it != (this->AdjacencyVector.at(a-1)).end()) {canadd = false;}
  else
    // Have to check if addition possible
    {
      Ca.insert(a); Cb.insert(b);
      jt_a = FindCliqueContaining(Ca);
      jt_b = FindCliqueContaining(Cb);
      return_pair = DFSJunctionTree(jt_a, jt_b);
      samecc = return_pair.first;
      if(!samecc)
	{
	  canadd = true;
	  // different connected components, so make a new clique
	  // attach to arbitrary cliques containing a and b
	  {
	    if(jt_a->C.size() == 1 && jt_b->C.size() == 1)
	      // both are singleton cliques, amalgamate and delete originals
	      {
		jt_a->C.insert(b);
		this->JunctionForest.erase(jt_b);
	      }
	    else if(jt_a->C.size() == 1 && jt_b->C.size() != 1)
	      // a is a singleton clique, add b to jt_a and attach to jt_b
	      {
		jt_a->C.insert(b);
		jt_a->Neighbors.push_back(jt_b);
		jt_b->Neighbors.push_back(jt_a);
	      }
	    else if(jt_a->C.size() != 1 && jt_b->C.size() == 1)
	      // b is a singleton clique, add a to jt_b and attach to jt_a
	      {
		jt_b->C.insert(a);
		jt_b->Neighbors.push_back(jt_a);
		jt_a->Neighbors.push_back(jt_b);
	      }
	    else
	      // neither are singleton cliques
	      // make a new clique and attach edges to jt_a and jt_b
	      {
		list<JunctionTreeNode>::iterator jt_ab;
		NodeSet NewClique; NewClique.insert(a); NewClique.insert(b);
		JunctionTreeNode JTN; JTN.C = NewClique;
		jt_ab = this->JunctionForest.insert(jt_b, JTN);
		jt_ab->Neighbors.push_back(jt_a);
		jt_ab->Neighbors.push_back(jt_b);
		jt_a->Neighbors.push_back(jt_ab);
		jt_b->Neighbors.push_back(jt_ab);
	      }

	  }
	}
      else
	// In same connected component
	// Use Berry (2006) condition to check if addition possible
	{
	  if(Sep.empty()) {canadd = false;}
	  else
	    {
	      // We have path from Ca to Cb
	      // Working inwards from Ca to get clique containing b
	      // Similarly, work inwards from Cb to get clique containing a
	      list< list<JunctionTreeNode>::iterator>::reverse_iterator list_rit;
	      list_it = (return_pair.second).begin();
	      found = false;
	      while(list_it != (return_pair.second).end() && !found)
		{
		  set_it = (*list_it)->C.find(b);
		  if(set_it != (*list_it)->C.end())
		    {
		      // We've found the clique closest to Ca containing b
		      jt_b = *list_it;
		      found = true;
		    }
		  else list_it++;
		}
	      list_rit = (return_pair.second).rbegin();
	      found = false;
	      while(list_rit != (return_pair.second).rend() && !found)
		{
		  set_it = (*list_rit)->C.find(a);
		  if(set_it != (*list_rit)->C.end())
		    {
		      // We've found the clique closest to Cb containing a
		      jt_a = *list_rit;
		      found = true;
		    }
		  else list_rit++;
		}
	    }

	  // Now we have jt_a and jt_b
	  // First check if they are neighbors
	  bool neighbors = false, subfound = false;
	  list< list<JunctionTreeNode>::iterator >::iterator neighbor_it;
	  neighbor_it = jt_a->Neighbors.begin();
	  while(neighbor_it != jt_a->Neighbors.end() && !neighbors)
	    {
	      if( (*neighbor_it)->C == jt_b->C ) {neighbors = true;}
	      else {neighbor_it++;}
	    }
	  if(neighbors)
	    {
	      if( Intersection(jt_a->C, jt_b->C) == Sep)
		{
		  canadd = true;
		}
	      else canadd = false;
	    }
	  else
	    // Now we have to find the path in the junction tree connecting jt_a and jt_b
	    // Check if Sep is a separator along that path
	    {
	      return_pair = DFSJunctionTree(jt_a, jt_b);
	      list_it = ((return_pair.second).begin());
	      list_jt = list_it; list_jt++;
	      list<JunctionTreeNode>::iterator jt_i, jt_j;
	      containssep = false;
	      while(!containssep && list_jt != (return_pair.second).end())
		{
		  jt_i = *list_it; jt_j = *list_jt;
		  if( Intersection(jt_i->C, jt_j->C) == Sep)
		    {
		      containssep = true;
		      canadd=true;
		      // Now make jt_a and jt_b neighbors
		      // Delete jt_i and jt_j from each other's neighbor lists
		      // First one way
		      neighbor_it = jt_i->Neighbors.begin();
		      subfound = false;
		      while( neighbor_it != jt_i->Neighbors.end() && !subfound)
			{
			  if( (*neighbor_it)->C == jt_j->C)
			    {
			      subfound = true;
			      list< list<JunctionTreeNode>::iterator >::iterator neighbor_jt = neighbor_it;
			      jt_i->Neighbors.erase(neighbor_jt);
			      continue;
			    }
			  neighbor_it++;
			}
		      //And the other way....
		      neighbor_it = jt_j->Neighbors.begin();
		      subfound = false;
		      while( neighbor_it != jt_j->Neighbors.end() && !subfound)
			{
			  if( (*neighbor_it)->C == jt_i->C)
			    {
			      subfound = true;
			      list< list<JunctionTreeNode>::iterator >::iterator neighbor_jt = neighbor_it;
			      jt_j->Neighbors.erase(neighbor_jt);
			      continue;
			    }
			  neighbor_it++;
			}
		      jt_a->Neighbors.push_back(jt_b);
		      jt_b->Neighbors.push_back(jt_a);
		    }
		  else{list_it++; list_jt++;}
		}
	    }
	}
      if(canadd && samecc)
	{
	  Ca = jt_a->C; Cb = jt_b->C;
	  Ca.erase(a); Cb.erase(b);
	  for(set<ushort>::iterator set_it = Sep.begin(); set_it != Sep.end(); set_it++)
	    {
	      Ca.erase(*set_it);
	      Cb.erase(*set_it);
	    }
	  if(Ca.empty() && Cb.empty())
	    // Amalgamate the cliques
	    {
	      JunctionTreeNode JTN; JTN.C = jt_b->C; (JTN.C).insert(a);
	      list<JunctionTreeNode>::iterator jt_ab = this->JunctionForest.insert(jt_b, JTN);
	      // First change the reference to jt_b on the neighbor list of jt_b
	      list < list<JunctionTreeNode>::iterator>::iterator neighbor_it;
	      for( neighbor_it = jt_b->Neighbors.begin(); neighbor_it != jt_b->Neighbors.end(); neighbor_it++)
		{
		  if( (*neighbor_it)->C != jt_a->C )
		    {
		      subfound = false;
		      list < list<JunctionTreeNode>::iterator>::iterator neighbor_jt = (*neighbor_it)->Neighbors.begin();
		      while(neighbor_jt != (*neighbor_it)->Neighbors.end() && ! subfound)
			{
			  if( (*neighbor_jt)->C == jt_b->C )
			    {
			      subfound = true;
			      *neighbor_jt = jt_ab;
			    }
			  else{neighbor_jt++;}
			}
		      jt_ab->Neighbors.push_back(*neighbor_it);
		    }
		}

	      for( neighbor_it = jt_a->Neighbors.begin(); neighbor_it != jt_a->Neighbors.end(); neighbor_it++)
		{
		  if( (*neighbor_it)->C != jt_b->C )
		    {
		      subfound = false;
		      list < list<JunctionTreeNode>::iterator>::iterator neighbor_jt = (*neighbor_it)->Neighbors.begin();
		      while(neighbor_jt != (*neighbor_it)->Neighbors.end() && ! subfound)
			{
			  if( (*neighbor_jt)->C == jt_a->C )
			    {
			      subfound = true;
			      *neighbor_jt = jt_ab;
			    }
			  else{neighbor_jt++;}
			}
		      jt_ab->Neighbors.push_back(*neighbor_it);
		    }
		}
	      this->JunctionForest.erase(jt_a);
	      this->JunctionForest.erase(jt_b);
	    }

	  else if(Ca.empty() && !Cb.empty())
	    {
	      jt_a->C.insert(b);
	    }

	  else if(!Ca.empty() && Cb.empty())
	    {
	      jt_b->C.insert(a);
	    }

	  else
	    // Neither is empty
	    // add a new clique comprising a U b U sep and add edges to jt_a and jt_b
	    {
	      NodeSet NewClique = Sep; NewClique.insert(a); NewClique.insert(b);
	      JunctionTreeNode JTN; JTN.C = NewClique;
	      list<JunctionTreeNode>::iterator jt_ab;
	      jt_ab = this->JunctionForest.insert(jt_b,JTN);
	      jt_ab->Neighbors.push_back(jt_a);
	      jt_ab->Neighbors.push_back(jt_b);
	      list< list<JunctionTreeNode>::iterator >::iterator neighbor_kt;
	      subfound = false;
	      neighbor_kt = jt_a->Neighbors.begin();
	      while( neighbor_kt != jt_a->Neighbors.end() && !subfound)
		{
		  if( (*neighbor_kt)->C == jt_b->C)
		    {
		      subfound = true;
		      *neighbor_kt = jt_ab;
		      continue;
		    }
		  else {neighbor_kt++;}
		}
	      subfound = false;
	      neighbor_kt = jt_b->Neighbors.begin();
	      while( neighbor_kt != jt_b->Neighbors.end() && !subfound)
		{
		  if( (*neighbor_kt)->C == jt_a->C)
		    {
		      subfound = true;
		      *neighbor_kt = jt_ab;
		      continue;
		    }
		  else{neighbor_kt++;}
		}
	    }
	}
      if(canadd)
	{
	  // Now have to update the adjacency vector and log score
	  this->nEdges++;
	  (this->AdjacencyVector.at(a-1)).insert(b);
	  (this->AdjacencyVector.at(b-1)).insert(a); 
	  this->logscore += ComponentLogRatioFBF(Cq, Sq2, Cq1, Cq2);

          // If doing multiplicity correction include below
          this->logscore += log((double)(this->nEdges)) - log((double)(this->MaxEdges - this->nEdges + 1.0));
	  //this->logscore += WongLogRatioFBF(a,b,Sq2);
	}
    }
  return canadd;
}


void DecomposableGraph::RemoveEdgeFBF(ushort a, ushort b)
{
  NodeSet K, Cq, Cq1, Cq2, Sq2;
  K.insert(a); K.insert(b);
  list<JunctionTreeNode>::iterator jt_ab, jt_a, jt_b;
  list< list<JunctionTreeNode>::iterator >::iterator neighbor_it, neighbor_jt, neighbor_lt;
  bool containsa, containsb;
  bool found;
  jt_ab = this->FindCliqueContaining(K);
  // Below are the four guys needed to update the model score
  // WLOG, a is node i in theorem 3.1 of Carlos's thesis
  // hence Node b is not contained in what separates this node from the earlier nodes in the perfect order
  // There is no loss of generality since perfect orders can start from any node in the junction tree
  Cq = jt_ab->C;
  //cout << "Contained in Clique: "; NodeSetPrint(jt_ab->C); cout << endl;
  Cq1 = Cq; Cq1.erase(b);
  Cq2 = Cq; Cq2.erase(a);
  Sq2 = Cq2; Sq2.erase(b);

  // Now look around the neighbors of jt_ab to see if any contain a or b
  neighbor_it = jt_ab->Neighbors.begin();
  containsa = false; containsb = false;
  list< list<JunctionTreeNode>::iterator >::iterator erase_it, erase_jt;
  while(neighbor_it != jt_ab->Neighbors.end() && (!containsa || !containsb))
    {
      if(!containsa && (Intersection(Cq1, (*neighbor_it)->C) == Cq1) )
	{
	  //cout << "a collapses into clique: "; NodeSetPrint( (*neighbor_it)->C); cout << endl;
	  containsa = true;
	  jt_a = *neighbor_it;
	  // Now delete jt_ab from this guy's neighbor list
	  found = false;
	  neighbor_jt = jt_a->Neighbors.begin();
	  while(neighbor_jt != jt_a->Neighbors.end() && !found)
	    {
	      if( *neighbor_jt == jt_ab)
		{
		  found = true;
		  list< list<JunctionTreeNode>::iterator >::iterator neighbor_lt = neighbor_jt;
		  jt_a->Neighbors.erase(neighbor_lt);
		}
	      neighbor_jt++;
	    }
	  erase_it = neighbor_it;
	}
      else if(!containsb && (Intersection(Cq2, (*neighbor_it)->C) == Cq2) )
	{
	  //cout << "b collapses into clique: "; NodeSetPrint( (*neighbor_it)->C); cout << endl;
	  containsb = true;
	  jt_b = *neighbor_it;
	  found = false;
	  neighbor_jt = jt_b->Neighbors.begin();
	  while(neighbor_jt != jt_b->Neighbors.end() && !found)
	    {
	      if( (*neighbor_jt)->C == jt_ab->C)
		{
		  found = true;
		  list< list<JunctionTreeNode>::iterator >::iterator neighbor_mt = neighbor_jt;
		  jt_b->Neighbors.erase(neighbor_mt);
		}
	      neighbor_jt++;
	    }
	  erase_jt = neighbor_it;
	}
      neighbor_it++;
    }

  if(containsa) {jt_ab->Neighbors.erase(erase_it);}
  if(containsb) {jt_ab->Neighbors.erase(erase_jt);}

  if(!containsa)
    {
      JunctionTreeNode JTNA;
      JTNA.C = Cq1;
      jt_a = this->JunctionForest.insert(jt_ab, JTNA);
    }
  if(!containsb)
    {
      JunctionTreeNode JTNB;
      JTNB.C = Cq2;
      jt_b = this->JunctionForest.insert(jt_ab, JTNB);
    }

  // Now go through the remaining neighbors of jt_ab and see which of jt_a, jt_b they should point to
  NodeSet TestA, TestB;
  for(neighbor_it = jt_ab->Neighbors.begin(); neighbor_it != jt_ab->Neighbors.end(); neighbor_it++)
    {
      containsa = false; containsb = false;
      //cout << "Looking at neighbor "; NodeSetPrint( (*neighbor_it)->C); cout << endl;
      containsa = IsSubset(a, (*neighbor_it)->C);
      containsb = IsSubset(b, (*neighbor_it)->C);
      if(containsa && containsb) {cout << "Error: Junction tree corrupted or tried to delete an inappropriate edge." << endl; exit(1);}
      else if(containsa)
	{
	  //cout << "\tNeighbor goes to jt_a " << endl;
	  // First change the reference in the neighbor's neighbor list
	  found = false;
	  neighbor_jt = (*neighbor_it)->Neighbors.begin();
	  while(neighbor_jt != (*neighbor_it)->Neighbors.end() && !found)
	    {
	      if( (*neighbor_jt)->C == jt_ab->C )
		{
		  found = true;
		  // Change the reference from jt_ab to jt_a
		  *neighbor_jt = jt_a;
		}
	      else {neighbor_jt++;}
	    }
	  // Insert a reference to this guy into jt_a's neighbors
	  jt_a->Neighbors.push_back(*neighbor_it);
	}
      else if(containsb)
	{
	  //cout << "\tNeighbor goes to jt_b " << endl;
	  // First change the reference in the neighbor's neighbor list
	  found = false;
	  neighbor_jt = (*neighbor_it)->Neighbors.begin();
	  while(neighbor_jt != (*neighbor_it)->Neighbors.end() && !found)
	    {
	      if( (*neighbor_jt)->C == jt_ab->C )
		{
		  found = true;
		  // Change the reference from jt_ab to jt_b
		  *neighbor_jt = jt_b;
		}
	      else {neighbor_jt++;}
	    }
	  // Insert a reference to this guy into jt_b's neighbors
	  jt_b->Neighbors.push_back(*neighbor_it);
	}
      else
	{
	  TestA = Intersection((*neighbor_it)->C, jt_a->C);
	  TestB = Intersection((*neighbor_it)->C, jt_b->C);
	  if(TestA == Intersection((*neighbor_it)->C, jt_ab->C))
	    {
	      //cout << "\tNeighbor goes to jt_a " << endl;
	      // First change the reference in the neighbor's neighbor list
	      found = false;
	      neighbor_jt = (*neighbor_it)->Neighbors.begin();
	      while(neighbor_jt != (*neighbor_it)->Neighbors.end() && !found)
		{
		  if( (*neighbor_jt)->C == jt_ab->C )
		    {
		      found = true;
		      // Change the reference from jt_ab to jt_a
		      *neighbor_jt = jt_a;
		    }
		  else {neighbor_jt++;}
		}
	      // Insert a reference to this guy into jt_a's neighbors
	      jt_a->Neighbors.push_back(*neighbor_it);
	    }
	  else if(TestB == Intersection((*neighbor_it)->C, jt_ab->C))
	    {
	      //cout << "\tNeighbor goes to jt_b " << endl;
	      // First change the reference in the neighbor's neighbor list
	      found = false;
	      neighbor_jt = (*neighbor_it)->Neighbors.begin();
	      while(neighbor_jt != (*neighbor_it)->Neighbors.end() && !found)
		{
		  if( (*neighbor_jt)->C == jt_ab->C )
		    {
		      found = true;
		      // Change the reference from jt_ab to jt_b
		      *neighbor_jt = jt_b;
		    }
		  else {neighbor_jt++;}
		}
	      // Insert a reference to this guy into jt_b's neighbors
	      jt_b->Neighbors.push_back(*neighbor_it);
	    }
	  else
	    {
	      cout << "Error in deletion: neighbor of modified clique doesn't seem to belong here...." << endl; exit(1);
	    }
	}
    }
  this->JunctionForest.erase(jt_ab);
  // Now finally add an edge between the two new nodes
  if(!Sq2.empty())
    {
      jt_a->Neighbors.push_back(jt_b);
      jt_b->Neighbors.push_back(jt_a);
    }

  // Now have to update the adjacency vector and log score
  (this->AdjacencyVector.at(a-1)).erase(b);
  (this->AdjacencyVector.at(b-1)).erase(a);
  this->nEdges--;
  this->logscore -= ComponentLogRatioFBF(Cq, Sq2, Cq1, Cq2);
  //this->logscore -= WongLogRatioFBF(a,b,Sq2);
  // If doing multiplicity correction include below
  this->logscore += log((double)(GGM::MaxEdges - this->nEdges)) - log((double)(this->nEdges + 1));
  // BECAUSE WONG IS WRONG!!
}


void DecomposableGraph::InitializeGraphFBF(EdgeList E, bool triangulate)
{
  // First initialize the edges
  std::list<Edge>::iterator list_it;
  Edge e;
  this->nEdges = 0;
  for(list_it = E.begin(); list_it != E.end(); list_it++)
    {
      e = *list_it;
      (AdjacencyVector.at(e.first - 1)).insert(e.second);
      (AdjacencyVector.at(e.second - 1)).insert(e.first);
    }

  // Now do a PEO and construct the Junction Tree
  if(triangulate) {this->Triangulate();}
  // Now count how many edges
  for(ushort i = 0; i < this->nNodes; i++)
    {
      this->nEdges = this->nEdges + (AdjacencyVector.at(i)).size();
    }
  this->nEdges = this->nEdges / 2;
  this->MakeJunctionTreeFBF();
  this->logscore += (0.0-log((double)this->MaxEdges + 1.0)) + lgamma(this->MaxEdges+1.0) - lgamma(this->nEdges+1.0) - lgamma(this->MaxEdges - this->nEdges + 1.0);
}


void DecomposableGraph::Triangulate(void)
{
  ushort N = AdjacencyVector.size();
  vector<NodeSet>::iterator current_it;
  map< ushort, PEOProperties > PEOMap;
  std::map< ushort, PEOProperties >::iterator peo_it, peo_it2;
  NodeSet CurrentNeighbors;
  std::set< ushort >::iterator neighbor_it, set_it1, set_it2;
  deque< WeightedNode > NodeQueue;

  // First zero out the weights, default-label the nodes
  // Also insert pointers to the weights in the Node Queue
  // The PEO Map has key = node id, value = pair(PEO Label, PEO weight)
  ushort i;
  for(i = 1; i <= N; i++)
    {
      PEOMap.insert( pair<ushort, PEOProperties> (i, pair<ushort,ushort>(0,0)) );
    }
  for(peo_it = PEOMap.begin(); peo_it != PEOMap.end(); peo_it++)
    {
      NodeQueue.push_back( pair<ushort, ushort*>(peo_it->first, &((peo_it->second).second)) );
    }

  ushort k;
  for(i = N; i >= 1; i--)
    {
      // Bring the node of highest weight to the front of the queue
      make_heap(NodeQueue.begin(), NodeQueue.end(), CompareByWeightPointer);
      // Pick the node at the front of the queue
      k = (NodeQueue.begin())->first;
      peo_it = PEOMap.find(k);
      // Delete this node from the weight map since we're now labeling it
      NodeQueue.pop_front();
      // Now increment the weights on the unlabelled neighbors of the node we've picked
      current_it = this->AdjacencyVector.begin() + k-1;
      for(neighbor_it = current_it->begin(); neighbor_it != current_it->end(); neighbor_it++)
	{
	  peo_it2 = PEOMap.find(*neighbor_it);
	  if((peo_it2->second).first == 0)
	    {
	      ((peo_it2->second).second)++;
	    }
	}
      // Finally label the node we've chosen
      (peo_it->second).first = i;
    }

  // Now initialize the queue holding the nodes in PEO
  // This time node queue holds pair(node id, PEO label)
  NodeQueue.clear();
  for(peo_it = PEOMap.begin(); peo_it != PEOMap.end(); peo_it++)
    {
      NodeQueue.push_back( pair<ushort, ushort*> (peo_it->first, &((peo_it->second).first) ) );
    }
  sort(NodeQueue.begin(),NodeQueue.end(),CompareByWeightPointer);
  // Now the nodes should be in elimination order

  std::deque<WeightedNode>::iterator label_it;
  map<ushort, NodeSet> EliminationGraph;
  map<ushort, NodeSet>::iterator adj_it1, adj_it2;
  deque<NodeSet> FoundCliques;
  deque<NodeSet>::iterator found_it;
  bool subset;

  for(i=1; i<=N; i++)
    {
      EliminationGraph.insert(pair<ushort, NodeSet> (i, this->AdjacencyVector.at(i-1)) );
    }
  for(label_it = NodeQueue.begin(); label_it != NodeQueue.end(); label_it++)
    {
      subset = false;
      k = label_it->first; // This is the current node
      adj_it1 = EliminationGraph.find(k); CurrentNeighbors = adj_it1->second;
      for(neighbor_it = CurrentNeighbors.begin(); neighbor_it != CurrentNeighbors.end(); neighbor_it++)
	{
	  set_it1 = neighbor_it; set_it1++;
	  while(set_it1 != CurrentNeighbors.end())
	    {
	      (EliminationGraph.find(*set_it1))->second.insert(*neighbor_it);
	      (EliminationGraph.find(*neighbor_it))->second.insert(*set_it1);
	      (this->AdjacencyVector.at(*set_it1 - 1)).insert(*neighbor_it);
	      (this->AdjacencyVector.at(*neighbor_it - 1)).insert(*set_it1);
	      set_it1++;
	    }
	}
      // Now eliminate that guy from the graph
      for(neighbor_it = adj_it1->second.begin(); neighbor_it != adj_it1->second.end(); neighbor_it++)
	{
	  adj_it2 = EliminationGraph.find(*neighbor_it);
	  adj_it2->second.erase(k);
	}
      EliminationGraph.erase(adj_it1);
    }
}



void DecomposableGraph::MakeJunctionTreeFBF(void)
{
  ushort N = AdjacencyVector.size();
  vector<NodeSet>::iterator current_it;
  map< ushort, PEOProperties > PEOMap;
  std::map< ushort, PEOProperties >::iterator peo_it, peo_it2;
  NodeSet CurrentNeighbors;
  std::set< ushort >::iterator neighbor_it, set_it1, set_it2;
  deque< WeightedNode > NodeQueue;

  // First zero out the weights, default-label the nodes
  // Also insert pointers to the weights in the Node Queue
  // The PEO Map has key = node id, value = pair(PEO Label, PEO weight)
  ushort i;
  for(i = 1; i <= N; i++)
    {
      PEOMap.insert( pair<ushort, PEOProperties> (i, pair<ushort,ushort>(0,0)) );
    }
  for(peo_it = PEOMap.begin(); peo_it != PEOMap.end(); peo_it++)
    {
      NodeQueue.push_back( pair<ushort, ushort*>(peo_it->first, &((peo_it->second).second)) );
    }

  ushort k;
  for(i = N; i >= 1; i--)
    {
      // Bring the node of highest weight to the front of the queue
      make_heap(NodeQueue.begin(), NodeQueue.end(), CompareByWeightPointer);
      // Pick the node at the front of the queue
      k = (NodeQueue.begin())->first;
      peo_it = PEOMap.find(k);
      // Delete this node from the weight map since we're now labeling it
      NodeQueue.pop_front();
      // Now increment the weights on the unlabelled neighbors of the node we've picked
      current_it = this->AdjacencyVector.begin() + k-1;
      for(neighbor_it = current_it->begin(); neighbor_it != current_it->end(); neighbor_it++)
	{
	  peo_it2 = PEOMap.find(*neighbor_it);
	  if((peo_it2->second).first == 0)
	    {
	      ((peo_it2->second).second)++;
	    }
	}
      // Finally label the node we've chosen
      (peo_it->second).first = i;
    }


  // Now start building the junction tree
  // Initialize the queue holding the nodes in PEO
  // This time node queue holds pair(node id, PEO label)
  NodeQueue.clear();
  for(peo_it = PEOMap.begin(); peo_it != PEOMap.end(); peo_it++)
    {
      NodeQueue.push_back( pair<ushort, ushort*> (peo_it->first, &((peo_it->second).first) ) );
    }
  sort(NodeQueue.begin(),NodeQueue.end(),CompareByWeightPointer);
  // Now the nodes should be in perfect elimination order

  std::deque<WeightedNode>::iterator label_it;
  map<ushort, NodeSet> EliminationGraph;
  map<ushort, NodeSet>::iterator adj_it1, adj_it2;
  deque<NodeSet> FoundCliques;
  deque<NodeSet>::iterator found_it;
  bool subset;

  // Now start building cliques
  for(i=1; i<=N; i++)
    {
      EliminationGraph.insert(pair<ushort, NodeSet> (i, (this->AdjacencyVector).at(i-1)) );
    }
  this->JunctionForest.clear();
  JunctionTreeNode JTN;
  for(label_it = NodeQueue.begin(); label_it != NodeQueue.end(); label_it++)
    {
      subset = false;
      k = label_it->first; // This is the current node
      adj_it1 = EliminationGraph.find(k);
      CurrentNeighbors = adj_it1->second;
      CurrentNeighbors.insert(k);
      found_it = FoundCliques.begin();
      while(subset == false && found_it != FoundCliques.end())
	{
	  subset = IsSubset(CurrentNeighbors,*found_it);
	  found_it++;
	}
      if(!subset)
	{
	  JTN.C = CurrentNeighbors;
	  JTN.SearchColor = false;
	  this->JunctionForest.push_front(JTN);
	  FoundCliques.push_front(CurrentNeighbors);
	}
      // Now eliminate that guy from the graph
      for(neighbor_it = (adj_it1->second).begin(); neighbor_it != (adj_it1->second).end(); neighbor_it++)
	{
	  adj_it2 = EliminationGraph.find(*neighbor_it);
	  adj_it2->second.erase(k);
	}
      EliminationGraph.erase(adj_it1);
    }


  // Now make build the junction tree
  list<JunctionTreeNode>::iterator jt_i, jt_j;
  NodeSet FoundSet, Clique, Separator;
  bool found, connected;
  this->logscore = - (((double)(GGM::nObs) * ((double)(GGM::nNodes))) / 2.0) * LOGTWOPI;
  jt_i = this->JunctionForest.begin();
  Clique = jt_i->C;
  for(neighbor_it = Clique.begin(); neighbor_it != Clique.end(); neighbor_it++)
    {
      FoundSet.insert(*neighbor_it);
    }
  this->logscore += CliqueScoreFBF(Clique);
  jt_i++;
  while(jt_i != this->JunctionForest.end())
    {
      Clique = jt_i->C;
      this->logscore += CliqueScoreFBF(Clique);
      Separator = Intersection(Clique, FoundSet);
      this->logscore -= CliqueScoreFBF(Separator);
      jt_j = this->JunctionForest.begin();
      found = false;
      connected = true;
      while(jt_j != jt_i && !found && connected)
	{
	  if(Separator.empty()) {connected = false;}
	  else {found = IsSubset(Separator, jt_j->C);}
	  if(found && connected)
	    {
	      jt_i->Neighbors.push_back(jt_j);
	      jt_j->Neighbors.push_back(jt_i);
	    }
	  else {jt_j++;}
	}
      for(neighbor_it = Clique.begin(); neighbor_it != Clique.end(); neighbor_it++)
	{
	  FoundSet.insert(*neighbor_it);
	}
      jt_i++;
    }

  // This section prints out the junction tree

  //   list< list<JunctionTreeNode>::iterator>::iterator print_it;
  //   for(jt_i = this->JunctionForest.begin(); jt_i != this->JunctionForest.end(); jt_i++)
  //     {
  //       NodeSetPrint(jt_i->C);
  //       cout << ": ";
  //       for(print_it = jt_i->Neighbors.begin(); print_it != jt_i->Neighbors.end(); print_it++)
  // 	{
  // 	  NodeSetPrint((*print_it)->C);
  // 	  cout << ", ";
  // 	}
  //       cout << endl;
  //     }
}


void DecomposableGraph::Complete(NodeSet K)
{
  set<ushort>::iterator set_it1, set_it2;
  for(set_it1 = K.begin(); set_it1 != K.end(); set_it1++)
    {
      set_it2 = set_it1; set_it2++;
      while(set_it2 != K.end())
	{
	  ((this->AdjacencyVector).at(*set_it1 - 1)).insert(*set_it2);
	  ((this->AdjacencyVector).at(*set_it2 - 1)).insert(*set_it1);
	  set_it2++;
	}
    }
}

void DecomposableGraph::ComputeScoreFBF(void)
{
  list< JunctionTreeNode >::iterator jt_i;
  this->logscore=0.0;
  for(jt_i = this->JunctionForest.begin(); jt_i != this->JunctionForest.end(); jt_i++)
    {
      this->logscore += CliqueScoreFBF(jt_i->C);
    }
}


bool DecomposableGraph::AreConnected(ushort a, ushort b)
{
  NodeSet Ca, Cb; Ca.insert(a); Cb.insert(b);
  list<JunctionTreeNode>::iterator jt_a = FindCliqueContaining(Ca);
  list<JunctionTreeNode>::iterator jt_b = FindCliqueContaining(Cb);
  pair<bool, list< list<JunctionTreeNode>::iterator> > return_pair;
  return_pair = this->DFSJunctionTree(jt_a, jt_b);
  return return_pair.first;
}


bool DecomposableGraph::DFSNode(list<JunctionTreeNode>::iterator jt_i, ushort n)
{
  pair<bool, list< list<JunctionTreeNode>::iterator> > search_pair;
  search_pair = DFSNodeRecursion(jt_i, n);
  list< list<JunctionTreeNode>::iterator>::iterator list_it;
  for(list_it = search_pair.second.begin(); list_it != search_pair.second.end(); list_it++)
    {
      (*list_it)->SearchColor = false;
    }
  return search_pair.first;
}


pair<bool, list< list<JunctionTreeNode>::iterator> > DecomposableGraph::DFSNodeRecursion(list<JunctionTreeNode>::iterator jt_i, ushort n)
{
  list< list<JunctionTreeNode>::iterator>::iterator list_it, list_it2;
  list< list<JunctionTreeNode>::iterator> HereColoredList;
  pair< bool, list< list<JunctionTreeNode>::iterator> > BoolPlusList;
  bool found;
  found = IsSubset(n, jt_i->C);
  if(!found)
    {
      jt_i->SearchColor = true;
      HereColoredList.push_back(jt_i);
      list_it = jt_i->Neighbors.begin();
      while(list_it != jt_i->Neighbors.end() && !found)
	{
	  if( (*list_it)->SearchColor == true) {list_it++; continue;}
	  BoolPlusList = DFSNodeRecursion(*list_it, n);
	  found = BoolPlusList.first;
	  for(list_it2 = BoolPlusList.second.begin(); list_it2 != BoolPlusList.second.end(); list_it2++)
	    {
	      HereColoredList.push_back(*list_it2);
	    }
	}
    }
  pair<bool, list< list<JunctionTreeNode>::iterator> > return_pair(found, HereColoredList);
  return(return_pair);
}



pair<bool, list< list<JunctionTreeNode>::iterator> > DecomposableGraph::DFSJunctionTreeRecursion(list< list<JunctionTreeNode>::iterator> CurrentList, list<JunctionTreeNode>::iterator current_it, list<JunctionTreeNode>::iterator target_it)
// This function is the recursive building block of a depth-first-search from one node to another on junction tree
// First argument is a list of previous junction tree nodes in the path from the source
// Second argument is the current jt node
// Third argument is the target node
{
  list< list<JunctionTreeNode>::iterator>::iterator list_it, list_jt;
  list< list<JunctionTreeNode>::iterator> ThisList = CurrentList;
  bool found = false, subfound = false;
  pair< bool, list< list<JunctionTreeNode>::iterator> > return_pair (found, ThisList);
  ThisList.push_back(current_it);

  if( current_it->C == target_it->C)
    {
      found = true;
    }

  else
    // Search among neighbors
    {
      list_it = current_it->Neighbors.begin();
      while(list_it != current_it->Neighbors.end() && !found)
	{
	  list_jt = CurrentList.begin(); subfound = false;
	  while( list_jt != CurrentList.end() && !subfound)
	    {
	      if( (*list_it)->C == (*list_jt)->C)
		{
		  subfound = true;
		}
	      list_jt++;
	    }
	  if(!subfound)
	    {
	      return_pair = this->DFSJunctionTreeRecursion(ThisList, *list_it, target_it);
	      found = return_pair.first;
	      ThisList = return_pair.second;
	    }
	  list_it++;
	}
    }
  if(!found)
    {
      ThisList.pop_back();
    }
  return_pair = pair< bool, list< list<JunctionTreeNode>::iterator> >(found, ThisList);
  return return_pair;
}

pair<bool, list< list<JunctionTreeNode>::iterator> > DecomposableGraph::DFSJunctionTree(list<JunctionTreeNode>::iterator source_it, list<JunctionTreeNode>::iterator target_it)
{
  list< list<JunctionTreeNode>::iterator> ThisList;
  pair<bool, list< list<JunctionTreeNode>::iterator> > return_pair;
  return_pair = this->DFSJunctionTreeRecursion(ThisList, source_it, target_it);
  list< list<JunctionTreeNode>::iterator >::iterator dfs_it;
  // cout << "Path from first node to second: " << endl;
  for(dfs_it = (return_pair.second).begin(); dfs_it != (return_pair.second).end(); dfs_it++)
    {
      (*dfs_it)->SearchColor = false;
      //      NodeSetPrint((*dfs_it)->C); cout << ", ";
    }
  //cout << endl;
  return return_pair;
}


ReturnMatrix DecomposableGraph::GetIncidence(void)
{
  SymmetricMatrix Incidence(this->nNodes); Incidence = 0.0;
  ushort i = 1;
  for(vector< NodeSet >::iterator jf_it = this->AdjacencyVector.begin(); jf_it != this->AdjacencyVector.end(); jf_it++)
    {
      for(set<ushort>::iterator set_it = jf_it->begin(); set_it != jf_it->end(); set_it++)
	{
	  Incidence(i,*set_it) = 1;
	}
      i++;
    }
  Incidence.Release();
  return Incidence.ForReturn();
}


bool DecomposableGraph::FINCSLocalMoveFBF(void)
{
  pair<int,int> edge;
  SymmetricMatrix PossibleMoves(this->nNodes);
  SymmetricMatrix MoveWeights(this->nNodes);
  // First choose to add or delete
  bool adding = false; bool deleting = false;
  bool foundedge = false;
  ushort i, j;
  if(this->nEdges == 0)
    {
      adding = true;
      deleting = false;
    }
  else if(this->nEdges == this->MaxEdges)
    {
      adding = false;
      deleting = true;
    }
  else
    {
      if( this->Random.uniform(0.0,1.0) > 0.5)
	{
	  adding = true;
	  deleting = false;
	}
      else
	{
	  adding = false;
	  deleting = true;
	}
    }
  if(adding)
    {
      //cout << "Adding" << endl;
      PossibleMoves = 1.0 - this->GetIncidence();
      for(i = 1; i <= this->nNodes; i++) {PossibleMoves(i,i) = 0.0;}
      for(i = 1; i <= this->nNodes; i++)
	{
	  for(j = 1; j < i; j++)
	    {
	      MoveWeights(i,j) = PossibleMoves(i,j) * ( (1.0 - 2.0*(this->adjust))*(this->Inclusion(i,j)) + (this->adjust) );
	    }
	}

      while(!foundedge)
	{
	  edge = SampleMatrix(MoveWeights);
	  foundedge = this->AddEdgeFBF(edge.first,edge.second);
	  if(!foundedge) MoveWeights(edge.first,edge.second) = 0;
	}

//       edge = SampleMatrix(MoveWeights);
//       foundedge = this->AddEdgeFBF(edge.first,edge.second);

    }
  else
    {
      //cout << "Deleting" << endl;
      PossibleMoves = this->GetIncidence();
      for(i = 1; i <= this->nNodes; i++)
	{
	  for(j = 1; j < i; j++)
	    {
	      MoveWeights(i,j) = PossibleMoves(i,j) * ( (1.0 - 2.0*(this->adjust))*(1.0 - this->Inclusion(i,j)) + (this->adjust) );
	    }
	}


      while(!foundedge)
	{
	  edge = SampleMatrix(MoveWeights);
	  foundedge = CanRemoveEdge(edge.first, edge.second);
	  if(!foundedge) MoveWeights(edge.first,edge.second) = 0;
	}
      if(foundedge) this->RemoveEdgeFBF(edge.first,edge.second);

//       edge = SampleMatrix(MoveWeights);
//       foundedge = CanRemoveEdge(edge.first, edge.second);
//       if(foundedge) this->RemoveEdgeFBF(edge.first,edge.second);

    }
  return foundedge;
}

bool DecomposableGraph::MetropolisMoveFBF(void)
{
  pair<int,int> edge;
  SymmetricMatrix PossibleMoves(this->nNodes);
  SymmetricMatrix MoveWeights(this->nNodes);
  // First choose to add or delete
  bool adding = false; bool deleting = false;
  bool foundedge = false;
  ushort i, j;
  if(this->nEdges == 0)
    {
      adding = true;
      deleting = false;
    }
  else if(this->nEdges == this->MaxEdges)
    {
      adding = false;
      deleting = true;
    }
  else
    {
      if( this->Random.uniform(0.0,1.0) > 0.5)
	{
	  adding = true;
	  deleting = false;
	}
      else
	{
	  adding = false;
	  deleting = true;
	}
    }
  if(adding)
    {
      //cout << "Adding" << endl;
      PossibleMoves = 1.0 - this->GetIncidence();
      for(i = 1; i <= this->nNodes; i++) {PossibleMoves(i,i) = 0.0;}
      for(i = 1; i <= this->nNodes; i++)
	{
	  for(j = 1; j < i; j++)
	    {
	      MoveWeights(i,j) = PossibleMoves(i,j) * 0.5;
	    }
	}


      while(!foundedge)
	{
	  edge = SampleMatrix(MoveWeights);
	  foundedge = this->AddEdgeFBF(edge.first,edge.second);
	  if(!foundedge) MoveWeights(edge.first,edge.second) = 0;
	}

//       edge = SampleMatrix(MoveWeights);
//       foundedge = this->AddEdgeFBF(edge.first,edge.second);

    }
  else
    {
      //cout << "Deleting" << endl;
      PossibleMoves = this->GetIncidence();
      for(i = 1; i <= this->nNodes; i++)
	{
	  for(j = 1; j < i; j++)
	    {
	      MoveWeights(i,j) = MoveWeights(i,j) = PossibleMoves(i,j) * 0.5;
	    }
	}

      while(!foundedge)
	{
	  edge = SampleMatrix(MoveWeights);
	  foundedge = CanRemoveEdge(edge.first, edge.second);
	  if(!foundedge) MoveWeights(edge.first,edge.second) = 0;
	}
      if(foundedge) this->RemoveEdgeFBF(edge.first,edge.second);


//       edge = SampleMatrix(MoveWeights);
//       foundedge = CanRemoveEdge(edge.first, edge.second);
//       if(foundedge) this->RemoveEdgeFBF(edge.first,edge.second);

    }
  return(foundedge);
}


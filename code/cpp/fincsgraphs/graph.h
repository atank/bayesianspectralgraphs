#ifndef GRAPH_H
#define GRAPH_H

#include <map>
#include <iostream>
#include <iomanip>
#include <utility>                   // for std::pair
#include <algorithm>                 // for std::for_each
#include <iterator>
#include "newmatap.h"
#include "newmatio.h"
#include "myexcept.h"
#include "rng.h"
#include "graphutilities.h"
#include "bayesian.h"



extern double adjust;
extern SymmetricMatrix EdgeWeights;


///////////////////////////
// Class definitions
///////////////////////////

using namespace std;


class GGM
{
 public:
  static ushort nNodes;
  static ushort nObs;
  static ushort MaxEdges;
  static ushort MaxCliqueSize;
  static Matrix X;
  static SymmetricMatrix EdgeWeights;
  static SymmetricMatrix Inclusion;
  static double g;
  static double tau;
  static double delta;
  static double pHat;
  static RNG Random;
  static double adjust;
  double logscore;
  ushort WhenFound;
  ushort nEdges;
  EdgeList GenerateStartingGraph(double threshold);
  //EdgeList GGM::GenerateStartingGraph(double threshold);
};


class JunctionTreeNode
{
 public:
  JunctionTreeNode(void) {SearchColor = false;}
  NodeSet C;
  list< list<JunctionTreeNode>::iterator > Neighbors;
  bool SearchColor;
  friend class JunctionTreeNodeCompare;
  friend class DecomposableGraph;
  friend void NodeSetPrint(NodeSet K);
  ~JunctionTreeNode(void);
};



struct JunctionTreeNodeCompare
// Ordered by their separators
{
  bool operator() (JunctionTreeNode lhs, JunctionTreeNode rhs)
  {
    return NodeSetCompare(lhs.C, rhs.C);
  }
};



class DecomposableGraph: public GGM
{
 public:
  DecomposableGraph(ushort n);
  DecomposableGraph(const DecomposableGraph& G);
  DecomposableGraph& operator=(const DecomposableGraph& G);
  void InitializeGraphFBF(EdgeList E, bool triangulate);
  void RandomMedianGraph(double tol);
  void Triangulate(void);
  void MakeJunctionTreeFBF(void);
  bool CanAddEdge(ushort a, ushort b);
  bool CanRemoveEdge(ushort a, ushort b);
  bool AddEdgeFBF(ushort a, ushort b);
  void RemoveEdgeFBF(ushort a, ushort b);
  NodeSet Neighbors(ushort n);
  ushort Size(void);
  void Complete(NodeSet K);
  bool AreConnected(ushort a, ushort b);
  list<JunctionTreeNode>::iterator FindCliqueContaining(NodeSet K);
  pair< bool, pair< list<JunctionTreeNode>::iterator, list<JunctionTreeNode>::iterator > > FindSeparator(NodeSet K);
  bool DFSNode(list<JunctionTreeNode>::iterator jt_i, ushort n);
  pair<bool, list< list<JunctionTreeNode>::iterator> > DFSNodeRecursion(list<JunctionTreeNode>::iterator jt_i, ushort n);
  pair<bool, list< list<JunctionTreeNode>::iterator> > DFSJunctionTreeRecursion(list< list<JunctionTreeNode>::iterator> CurrentList, list<JunctionTreeNode>::iterator current_it, list<JunctionTreeNode>::iterator target_it);
  pair<bool, list< list<JunctionTreeNode>::iterator> > DFSJunctionTree(list<JunctionTreeNode>::iterator source_it, list<JunctionTreeNode>::iterator target_it);
  void ComputeScoreFBF(void);
  void PrintGraph(void);
  void PrintJunctionTree(void);
  friend void NodeSetPrint(NodeSet K);
  ReturnMatrix GetIncidence(void);
  bool AreNeighbors(ushort a, ushort b);

  // Types of Moves!
  bool FINCSLocalMoveFBF(void);
  bool MetropolisMoveFBF(void);

 private:
  vector< NodeSet > AdjacencyVector;
  list< JunctionTreeNode > JunctionForest;
};


class GGMMap
{
 public:
  map< double, DecomposableGraph > ModelMap;
  double TopModels;
  double Correction;
};


#endif
  

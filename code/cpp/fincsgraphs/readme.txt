Details of the algorithm are in Scott and Carvalho, Feature-inclusion stochastic search for Gaussian graphical models, Journal of Computational and Graphical Statistics

Instructions for use

1) Create a text file with the following items in it, separated by spaces:

pathname of data file containing the data matrix X (n by p)
Number of observations/rows of X (n)
Number of nodes/columns of X (p)
Number of iterations
How often to make RMTP move (every nth iteration)
How often to resample an old model
Method to compute the marginal likelihoods
Method to compute prior model probabilities
Number of top models to keep in RAM
g (should typically be 1/n)
degrees of freedom in HIW prior (should be gn)
tau in conventional HIW(delta, tau * I) prior (not implemented)
prior inclusion probability
How often to print information (every nth iteration)

An example is given infile.txt, to accompany the data file MCorrectionp25.txt

2) Download and compile the Newmat Matrix C++ library into object code appropriate for your system.  http://www.robertnz.net/nm_intro.htm

3) After compiling Newmat, place the following objects into a directory ./newmat/ (with respect to the working directory where FINCS is to be compiled):

bandmat.o
svd.o
cholesky.o
evalue.o
fft.o
hholder.o
jacobi.o
myexcept.o
newfft.o
newmat1.o
newmat2.o
newmat3.o
newmat4.o
newmat5.o
newmat6.o
newmat7.o
newmat8.o
newmat9.o
newmatex.o
newmatnl.o
newmatrm.o
sort.o
submat.o

4) Type `make fincs' (without the quotation marks.

5) Pipe your infile into fincs using the command './fincs < infile' (without the quotation marks).

6) Output will be placed into the following files:

FINCSproperties.txt
FINCSinclusion.txt (edge inclusion probabilities)
FINCSlogscores.txt (best log posterior probabilities found)
FINCSbestgraphs.txt (incidence matrices of the best graphs found, same order as above)

FINCS depends upon an up-to-date version of the C++ standard template library (STL).

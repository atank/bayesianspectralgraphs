#include "bayesian.h"
#include "rng.h"

#include "util.h"

extern double g;
extern double delta;
extern double tau;
extern Matrix X;
extern ushort nObs;
extern RNG Random;

double log_gamma (double x)
{
  double z,y,tmp,ser;
  double coef[6] = {76.18009172947146,    -86.50532032941677,
                    24.01498240830910,    -1.231739572450155,
                    0.1208650973866179e-2,-0.5395239384953e-5};
  long j;

  y = x; z = x;
  tmp =  z+5.5;
  tmp -= (z+0.5)*log(tmp);
  ser =  1.000000000190015;
  for (j=0; j<=5; j++) ser+=coef[j]/++y;
  return (-tmp+log(2.5066282746310005*ser/z));
}


double MVlog_gamma (double arg,double dim)
{
  double x;
  int i;
  x = (dim*(dim-1.0)/4.0)*log(3.1415926535897);
  for(i=1;i<=dim;i++)
  {
    x+= log_gamma(arg-0.5*(double)(i-1.0));
  }

  return x;
}



double CliqueScoreFBF(NodeSet K)
{
    double s = 0.0;
    auto C = static_cast<double>(K.size());

    //std::cout << "Clique: (";
    //for (auto it = K.begin(); it != K.end(); ++it)
    //    std::cout << *it << " ";
    //std::cout << ")\n";

    if(C > 0)
    {
        set<ushort>::iterator set_it;
        Matrix Sub_r(K.size(), K.size());
        Matrix Sub_i(K.size(), K.size());

        for (unsigned int t = 0; t < GGM::T; t++)
        {
            // Construct submatrix from full matrix for this frequency
            
            ushort i = 1;
            for (auto it1 = K.begin(), end = K.end(); it1 != end; ++it1)
            {
                ushort j = 1;
                for (auto it2 = K.begin(); it2 != end; ++it2)
                {

                    Matrix *m_r = GGM::X_r_vec[t];
                    Matrix *m_i = GGM::X_i_vec[t];
                    Sub_r(i, j) = (*m_r)(*it1, *it2);
                    Sub_i(i, j) = (*m_i)(*it1, *it2);
                    j++;
                }
                i++;
            }

            // posterior part
            double ld = logdetc(Sub_r, Sub_i, K.size());
            s -= (GGM::df_vec[t] + C)*ld;
            s += C*((C-1)/2.)*log(PI);
            for (double i = 1; i <= C; i++)
                s += gammalnd(C + GGM::df_vec[t] - i + 1.);

            // prior part
            s += (GGM::df_vec[t]*GGM::g + C)*(C*log(GGM::g) + ld);
            s -= C*((C-1)/2.)*log(PI);
            for (double i = 1; i <= C; i++)
                s -= gammalnd(C + GGM::df_vec[t]*GGM::g - i + 1.);

        }

        //set<ushort>::iterator set_it;
        //Matrix SubX(GGM::nObs, K.size());
        //SymmetricMatrix D(K.size());
        //ushort i = 1;
        //for(set_it = K.begin(); set_it != K.end(); set_it++)
        //{
        //    SubX.Column(i) = (GGM::X).Column(*set_it);
        //    i++;
        //}
        //// First take care of the posterior part
        //D << 0.5*(SubX.t() * SubX);
        //double LDD = (D.LogDeterminant()).LogValue();
        //s -= ((double)((double)(GGM::nObs) + (double)K.size() - 1.0)/2.0)* LDD;
        //s += MVlog_gamma((double)((double)(GGM::nObs) + (double)K.size() - 1.0)/2.0, (double)K.size());
        //// Now the prior part
        //s += ((double)((double)(GGM::delta) + (double)K.size() - 1.0)/2.0)* ((double)K.size() * log(GGM::g) + LDD);
        //s -= MVlog_gamma((double)((double)(GGM::delta) + (double)K.size() - 1.0)/2.0, (double)K.size());
    }
    //std::cout << "  loglik: " << setprecision(16) << s << std::endl;
    return s;
}




double ComponentLogRatioFBF(NodeSet Cq, NodeSet Sq2, NodeSet Cq1, NodeSet Cq2)
{
  double s = 0.0;
  s += CliqueScoreFBF(Cq);
  s += CliqueScoreFBF(Sq2);
  s -= CliqueScoreFBF(Cq1);
  s -= CliqueScoreFBF(Cq2);
  return s;
}


double WongLogRatioFBF(ushort a, ushort b, NodeSet Sq2)
// Compute the log likelihood ratio of a bigger model to a smaller model
// where edge (a,b) has been removed
{
  double Cqsize = (double)Sq2.size() + 2.0;
  double Cq1size = Cqsize - 1.0;
  double Cq2size = Cq1size;
  double s = 0.0;
  double bprior = (double) GGM::delta;
  double bstar = (double) GGM::nObs;
  Matrix SubX(GGM::nObs, Sq2.size());
  int i = 1;
  for(set<ushort>::iterator set_it = Sq2.begin(); set_it != Sq2.end(); set_it++)
    {
      SubX.Column(i) = (GGM::X).Column(*set_it);
      i++;
    }

  SubX |= (GGM::X).Column(a);
  SubX |= (GGM::X).Column(b);

  // First the posterior part of the marginal likelihood ratio;
  SymmetricMatrix DQ(Sq2.size() + 2);
  DQ << (SubX.t() * SubX);
  LowerTriangularMatrix L = Cholesky(DQ);
  Matrix LQ = L.SubMatrix(Sq2.size() + 1, Sq2.size() + 2, Sq2.size() + 1, Sq2.size() + 2);
  double l_ii = LQ(1,1);
  double l_ji = LQ(2,1);
  double l_jj = LQ(2,2);

  s = 0.0 - ((bstar + Cqsize - 1.0)/2.0) * (2.0*log(l_ii) + 2.0*log(l_jj));
  s = s - MVlog_gamma( (bstar + Sq2.size())/2.0, (double)Sq2.size());
  s += ((bstar + Cq1size - 1.0)/2.0) * (2.0*log(l_ii));
  s += ((bstar + Cq2size - 1.0)/2.0) * log(pow(l_ji,2.0) + pow(l_jj,2.0));
  s += MVlog_gamma( ((bstar + (double)Sq2.size() + 1.0)/2.0), (double)Sq2.size());

  l_ii = l_ii*sqrt(GGM::g);
  l_ji = l_ji*sqrt(GGM::g);
  l_jj = l_jj*sqrt(GGM::g);

  s += ((bprior + Cqsize - 1.0)/2.0) * (2.0*log(l_ii) + 2.0*log(l_jj));
  s += MVlog_gamma( (bprior + (double)Sq2.size())/2.0, (double)Sq2.size());
  s = s - ((bprior + Cq1size - 1.0)/2.0) * (2.0*log(l_ii));
  s = s - ((bprior + Cq2size - 1.0)/2.0) * log(pow(l_ji,2.0) + pow(l_jj,2.0));
  s = s - MVlog_gamma((bprior + (double)Sq2.size() + 1.0)/2.0, (double)Sq2.size());

  return s;

}


pair<int, int> SampleMatrix(const SymmetricMatrix& X)
{
  int p = X.Nrows();
  double s = 0.0;
  int i = 2,j = 1;
  pair<int, int> edge(2,1);
  SymmetricMatrix CumSum(p); CumSum = 0;
  for(i = 2; i <= p; i++)
    {
      for(j = 1; j < i; j++)
	{
	  s += X(i,j);
	  CumSum(i,j) = s;
	}
    }
  double z = (GGM::Random).uniform(0.0,s);
  bool found = false;
  i = 2;
  while(i <= p && !found)
    {
      j = 1;
      while(j < i && !found)
	{
	  if( CumSum(i,j) > z)
	    {
	      found = true;
	      edge = pair<int,int>(i,j);
	    }
	  else {j++;}
	}
      i++;
    }
  return edge;
}

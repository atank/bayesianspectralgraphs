#ifndef BAYES_H
#define BAYES_H

#include "graphutilities.h"
#include "graph.h"
#include "rng.h"
#include <utility>


# define LOGTWOPI 1.83787706641

double log_gamma (double x);
double MVlog_gamma (double arg,double dim);
double CliqueScoreFBF(NodeSet K);
double ComponentLogRatioFBF(NodeSet Cq, NodeSet Sq2, NodeSet Cq1, NodeSet Cq2);
double WongLogRatioFBF(ushort a, ushort b, NodeSet Sq2);
pair<int, int> SampleMatrix(const SymmetricMatrix& X);

#endif

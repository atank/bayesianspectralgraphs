#ifndef GRAPHUTILS_H
#define GRAPHUTILS_H

using namespace std;

#include <utility>                   // for std::pair
#include <algorithm>                 // for std::for_each
#include <set>
#include <list>
#include <vector>
#include <queue>
#include <iostream>

typedef unsigned short ushort;
typedef pair<ushort, ushort> Edge;
typedef set<ushort> NodeSet;
typedef set<ushort>::iterator set_iterator;
typedef list<Edge> EdgeList;
typedef pair<ushort, ushort> PEOProperties;
typedef pair<ushort, ushort*> WeightedNode;

#define TOL 0.000001


///////////////////////////
// Function prototypes here
///////////////////////////

NodeSet SetTakeAway(NodeSet larger, NodeSet smaller);
NodeSet Intersection(NodeSet a, NodeSet b);
bool IsSubset(NodeSet smaller, NodeSet larger);
bool IsSubset(ushort a, NodeSet larger);
bool NodeSetCompare(NodeSet lhs, NodeSet rhs);
bool CompareByWeightPointer(WeightedNode a, WeightedNode b);
void NodeSetPrint(NodeSet a);
bool lesstol(double a, double b);

struct tolcomp
{
bool operator() (double lhs, double rhs){
return(lesstol(lhs,rhs));
}
} ;



#endif

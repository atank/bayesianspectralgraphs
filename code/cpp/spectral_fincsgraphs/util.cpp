
#include "util.h"

#include <iostream>
#include "newmatio.h"


double logdetc(const Matrix& R, const Matrix& I, unsigned int p)
{
    auto A = Matrix(2*p, 2*p);
    A.SubMatrix(1,p,1,p) = R;
    A.SubMatrix(1,p,p+1,2*p) = -I;
    A.SubMatrix(p+1,2*p,1,p) = I;
    A.SubMatrix(p+1,2*p,p+1,2*p) = R;

    return 0.5*A.LogDeterminant().LogValue();
}

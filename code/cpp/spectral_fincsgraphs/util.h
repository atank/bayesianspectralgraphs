#ifndef UTIL_H
#define UTIL_H

#include <boost/math/special_functions/gamma.hpp>

#include "newmatap.h"
#include "myexcept.h"

inline double gammalnd(double x) { return boost::math::lgamma<double>(x); }

double logdetc(const Matrix& R, const Matrix& I, unsigned int p);


#endif

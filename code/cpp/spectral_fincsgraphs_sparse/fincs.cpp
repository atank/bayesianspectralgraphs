
#include "fincs.h"

#define TOLORDERMAG 10

// Global declarations and initialization of static member variables
Matrix Temp;
Matrix GGM::X = Temp;
//
// spectral
unsigned int GGM::T = 0;
std::vector<Matrix*> GGM::X_r_vec;
std::vector<Matrix*> GGM::X_i_vec;
std::vector<double> GGM::df_vec;

ushort GGM::nObs = 0;
ushort GGM::nNodes = 0;
ushort GGM::MaxEdges = 0;
double GGM::e_prob = 0.5;
double GGM::g = 0.0;
double GGM::tau = 0.0;
double GGM::delta = 0.0;
double GGM::pHat = 0.5;
RNG GGM::Random;
SymmetricMatrix GGM::EdgeWeights;
SymmetricMatrix GGM::Inclusion;
double GGM::adjust = 0.05;

int main(void)
{
    // Declare some variables we need
    int verbose = 100;
    double** Data=NULL;
    double CurrentScore = 1.0, SumScores = 1.0, WorstScore = 1.0, BestScore = 1.0, bestlogscore = 0.0;
    char DataFile[512];
    char initGraphFile[512];
    FILE* in;
    int t, iterations;
    int coinflip, revisit;
    ushort LikeMethod, PriorMethod;
    GGMMap SavedModels;
    int TopModels;
    set<double, tolcomp> DiscoveredScores;
    pair<set<double,tolcomp>::iterator, bool> discovered;
    
    RNG Generator(rand());
    GGM::Random = Generator;
    
    // Scan values of variables from infile
    
    //  
    
    scanf("%s", DataFile);
    scanf("%s", initGraphFile);
    cin>>(GGM::T);           // Number of freequencies
    cin>>(GGM::nObs);        // Number of rows of X (NOT USED)
    cin>>(GGM::nNodes);	    // Number of columns of X
    cin>>(GGM::e_prob);     // Prior edge probability
    cin>>iterations;		    // Number of iterations
    cin>>coinflip;		    // How often to make RMTP move
    cin>>revisit;			    // How often to revisit a good model
    cin>>LikeMethod;		    // Method to compute the marginal likelihoods
    cin>>PriorMethod;         // Method to compute prior model probabilities
    cin>> TopModels;  	    // Number of top models to keep in RAM
    cin>> (GGM::g);           // Fractional prior parameters (fraction of data to use for prior in model selection)
    cin>>GGM::delta;          // Prior degrees of freedom parameter
    cin>>GGM::tau;            // Scale of centering matrix when no fractional prior
    cin>>GGM::pHat;           // common prior inclusion probability
    cin>>verbose;             // How often to print stuff
    
    if (iterations == 1)
    {
        std::cout << "iterations cannot be 1!!\n";
        exit(3);
    }
    
    // Initialize other static class variables
    GGM::MaxEdges = GGM::nNodes*(GGM::nNodes-1)/2;
    GGM::EdgeWeights = 0.0;
    GGM::Inclusion = 0.0;
    SavedModels.TopModels  = TopModels;
    
    //read in the real and imaginary portions of the data
    
    GGM::X_r_vec = std::vector<Matrix*>{GGM::T};
    GGM::X_i_vec = std::vector<Matrix*>{GGM::T};
    //GGM::df_vec = std::vector<double>{GGM::T};
    
    double **Data_r, **Data_i;
    Data_r = new double*[GGM::nNodes];
    Data_i = new double*[GGM::nNodes];
    for (int i = 0; i < GGM::nNodes; i++)
    {
        Data_r[i] = new double[GGM::nNodes];
        Data_i[i] = new double[GGM::nNodes];
    }
    
    std::string DataFile_str = string(DataFile);
    double s;
    FILE *real_in, *imag_in, *df_in;
    
    real_in = fopen((DataFile_str + ".real").c_str(), "r");
    if (!real_in)
    {
        printf("Cannot open data file %s.\n", (DataFile_str + ".real").c_str());
        exit(1);
    }
    imag_in = fopen((DataFile_str + ".imag").c_str(), "r");
    if (!imag_in)
    {
        printf("Cannot open data file %s.\n", (DataFile_str + ".imag").c_str());
        exit(1);
    }
    df_in = fopen((DataFile_str + ".df").c_str(), "r");
    if (!df_in)
    {
        printf("Cannot open data file %s.\n", (DataFile_str + ".df").c_str());
        exit(1);
    }
    
    // Loop over elements in files
    for (int t = 0; t < GGM::T; t++)
    {
        for (int i = 0; i < GGM::nNodes; i++)
        {
            for (int j = 0; j < GGM::nNodes; j++)
            {
                fscanf(real_in, "%lf", &s);
                Data_r[i][j] = s;
                //printf("Data_r[%d][%d]: %f\n", i, j, s);
                fscanf(imag_in, "%lf", &s);
                Data_i[i][j] = s;
                //printf("Data_i[%d][%d]: %f\n", i, j, s);
            }
        }
    
        Matrix *Xtemp = new Matrix(GGM::nNodes, GGM::nNodes);
        for (int i = 0; i < GGM::nNodes; i++)
            Xtemp->Row(i+1) << Data_r[i];
        GGM::X_r_vec[t] = Xtemp;
    
        Xtemp = new Matrix(GGM::nNodes, GGM::nNodes);
        for (int i = 0; i < GGM::nNodes; i++)
            Xtemp->Row(i+1) << Data_i[i];
        GGM::X_i_vec[t] = Xtemp;
    
        fscanf(df_in, "%lf", &s);
        GGM::df_vec.push_back(s);
    }
    fclose(real_in);
    fclose(imag_in);
    fclose(df_in);
 
    // Load the initial graph
    FILE *graph_in = fopen(initGraphFile, "r");
    EdgeList L;
    if(!graph_in)
    {
        printf("Cannot open graph file %s.\n", (DataFile_str + ".df").c_str());
        printf("  Using empty graph\n");
    }
    else
    {
        fseek(graph_in, 0, SEEK_END);
        cout << "File len: " << ftell(graph_in) << endl;
        if(!ftell(graph_in))
        {
            fseek(graph_in, 0, SEEK_SET);
            ushort src, dst;
            while (fscanf(graph_in, "%u %u", &src, &dst) != EOF)
                L.push_back(pair<ushort, ushort>(src, dst)); 
        }
        fclose(graph_in);
    }

    for (int i = 0; i < GGM::nNodes; i++)
    {
        delete [] Data_r[i];
        delete [] Data_i[i];
    }
    delete [] Data_r;
    delete [] Data_i;
    
    
    cout << "Loaded data.\n";
    cout << "T = " << setprecision(0) << GGM::T << endl;
    cout << "Nodes = " << setprecision(0) << GGM::nNodes << endl;
    cout << "Observations = " << setprecision(0) << GGM::nObs << endl;
    cout << "g = " << setprecision(0) << GGM::g << endl;
    cout << "delta = " << setprecision(0) << GGM::delta << endl;
    cout << "edge prob = " << setprecision(2) << GGM::e_prob << endl;
    
    RNG Random;
    
    // Initialize the model and append the first model to the saved models list
    DecomposableGraph CurrentGraph(GGM::nNodes);
    // Loaded from file now
    //EdgeList L;
    //L = CurrentGraph.GenerateStartingGraph(3.0);
    
    CurrentGraph.InitializeGraphFBF(L, true);
    cout << "Initial log marginal likelihood: " << setprecision(10) << CurrentGraph.logscore << endl;
    SavedModels.Correction = CurrentGraph.logscore;
    CurrentScore = exp(CurrentGraph.logscore - SavedModels.Correction);
    bestlogscore = CurrentGraph.logscore;
    (SavedModels.ModelMap).insert(pair<double, DecomposableGraph>(CurrentScore, CurrentGraph));
    GGM::EdgeWeights = CurrentGraph.GetIncidence();
    GGM::Inclusion = CurrentGraph.GetIncidence();
    //cout << "Initial graph:: " << endl;
    //cout << setprecision(0) << CurrentGraph.GetIncidence();
    
    double resamplekey;
    bool foundnew;
    map< double, DecomposableGraph >::iterator map_it;
    // Main loop begins here
    
    //DecomposableGraph BurnGraph(GGM::nNodes);
    //for(t=1; t<=100000; t++)
    //{
    //   DecomposableGraph NewGraph(GGM::nNodes);
    //   NewGraph = CurrentGraph;
    //   foundnew = NewGraph.MetropolisMoveFBF();
    //   if(foundnew && NewGraph.logscore - CurrentGraph.logscore > log(Random.uniform(0.0,1.0)))
    //   {
    //     CurrentGraph = NewGraph;
    //   }
    //   if(CurrentGraph.logscore > BurnGraph.logscore)
    //   {
    //     BurnGraph = CurrentGraph;
    //   }
    //   if(t % verbose == 0)
    //   {
    //     cout << "Burn-in: " << t << endl;
    //     cout << "Current best log score: " << setprecision(10) << BurnGraph.logscore << endl;
    //   }
    // }
    //CurrentGraph = BurnGraph;
    
    cout << "Initial log marginal likelihood: " << setprecision(10) << CurrentGraph.logscore << endl;
    SavedModels.Correction = CurrentGraph.logscore;
    CurrentScore = exp(CurrentGraph.logscore - SavedModels.Correction);
    bestlogscore = CurrentGraph.logscore;
    
    std::clock_t start = std::clock();
    for(t = 1; t <= iterations; t++)
    {
        // Just keep it from becoming 0
        GGM::adjust = (0.1)*(double)(iterations - t)/(double)iterations + 1e-22;
    
        if(t % verbose == 0)
        {
            cout << "Iteration " << t << endl;
            cout << "Current best log score: " << setprecision(10) << bestlogscore << endl;
            //cout << "Current best graph: " << endl;
            //cout << setprecision(0) << (SavedModels.ModelMap).rbegin()->second.GetIncidence() << endl;
        }
      
        if(t % coinflip == 0)
        {
            // Flip and triangulate
            CurrentGraph.RandomMedianGraph(1.0/(double)GGM::MaxEdges);
            cout << setprecision(0) << CurrentGraph.GetIncidence();
            for(int j = 0; j< (int)GGM::MaxEdges; j++)
            {
                DecomposableGraph NewGraph(GGM::nNodes);
                NewGraph = CurrentGraph;
                foundnew = NewGraph.MetropolisMoveFBF();
                if(foundnew && NewGraph.logscore - CurrentGraph.logscore > log(Random.uniform(0.0,1.0)))
                    CurrentGraph = NewGraph;
            }
        }
        else if(t % revisit == 0)
        {
            // resample an old good model
            // Then do a local move
            resamplekey = Random.uniform(0.0,BestScore);
            if(Random.uniform(0.0,1.0) > 0.9) resamplekey = BestScore;
            map_it = (SavedModels.ModelMap).lower_bound(resamplekey);
            CurrentGraph = map_it->second;
            foundnew = CurrentGraph.FINCSLocalMoveFBF();
        }
        else
        {
            // Local move only
            foundnew = CurrentGraph.FINCSLocalMoveFBF();
        }
      
        discovered.second = false;
        if(foundnew)
            discovered = DiscoveredScores.insert(CurrentGraph.logscore);
        if(discovered.second)
        {
            // We have a new model on our hands
            // Use it to update inclusion probabilities
            // Check if it belongs on the "Best-Of" list for resampling
            if(CurrentGraph.logscore - bestlogscore > TOLORDERMAG)
            {
                // We could have some numerical problems since the new model
                // is WAY WAY better than the old ones
                // So just start the accounting over!
                bestlogscore = CurrentGraph.logscore;
                GGM::EdgeWeights = CurrentGraph.GetIncidence();
                SavedModels.Correction = CurrentGraph.logscore;
                (SavedModels.ModelMap).clear();
                CurrentScore = 1.0;
                (SavedModels.ModelMap).insert(pair<double, DecomposableGraph>(CurrentScore, CurrentGraph));
                WorstScore = 1.0;
                SumScores = 1.0;
                BestScore = 1.0;
                GGM::Inclusion = GGM::EdgeWeights;
            }
            //else if(CurrentGraph.logscore - SavedModels.Correction > 10)
            //{
            //    // Renormalize
            //    CurrentScore = exp(CurrentGraph.logscore - SavedModels.Correction);
            //    SumScores += CurrentScore;
            //    GGM::EdgeWeights += ( max(0.0,CurrentScore) ) * CurrentGraph.GetIncidence();
            //}
            else
            {
                CurrentScore = exp(CurrentGraph.logscore - SavedModels.Correction);
                SumScores += CurrentScore;
                GGM::EdgeWeights += ( max(0.0,CurrentScore) ) * CurrentGraph.GetIncidence();
                GGM::Inclusion = GGM::EdgeWeights/SumScores;
                if(CurrentScore > BestScore)
                {
                    // We have a new best score
                    BestScore = CurrentScore;
                    bestlogscore = CurrentGraph.logscore;
                    SavedModels.ModelMap.insert(pair<double, DecomposableGraph>(CurrentScore, CurrentGraph));

                }
                else if(SavedModels.ModelMap.size() < SavedModels.TopModels || CurrentScore > WorstScore)
                {
                    // Better than the worst model on the resampling list
                    // Or we simply haven't saved enough models yet
                    SavedModels.ModelMap.insert(pair<double, DecomposableGraph>(CurrentScore, CurrentGraph));
                }
                if( (SavedModels.ModelMap).size() > SavedModels.TopModels)
                {
                    // We've got more than we want to save in the resample list
                    // Push the worst one off
                    SavedModels.ModelMap.erase( (SavedModels.ModelMap).begin() );
                }
            }
        }

        // Save current inclusion probabilities and a rounded version to file
        if (t % verbose == 0)
        {
            ofstream q_outfile;
            q_outfile.open((DataFile_str + ".inclusions").c_str());
            q_outfile << setw(5) << setprecision(3) << GGM::Inclusion;
            q_outfile.close();

            ofstream best_outfile;
            best_outfile.open((DataFile_str + ".bestgraph").c_str());
            best_outfile << std::fixed << setprecision(0) << GGM::Inclusion;
            best_outfile.close();
        }

    }  // End iterations loop
    
    // If we didn't save TopModels models, reset the number so we don't segfault
    SavedModels.TopModels = (SavedModels.ModelMap).size();
    std::cout << "Saving " << SavedModels.TopModels << " models: " << std::endl;
    
    // Clean up X_r_vec and X_i_vec
    for (int t = 0; t < GGM::T; t++)
    {
        delete GGM::X_r_vec[t];
        delete GGM::X_i_vec[t];
    }
    
    double runtime = ((std::clock() - start) / static_cast<double>(CLOCKS_PER_SEC));
    std::cout << "Runtime: " << setprecision(5) << runtime << " seconds\n";
    
    ofstream outfile;
    
    time_t curr;
    time(&curr);
    
    outfile.open("FINCSspectral_properties.txt");
    outfile << "Time of Run: " << ctime(&curr) << "\n";
    outfile << "LikeMethod: " << LikeMethod << "\n";
    outfile << "PriorMethod: " << PriorMethod << "\n";
    outfile << "Iterations: " << iterations << "\n";
    outfile << "Models Visited: " << DiscoveredScores.size() << "\n";
    outfile << "Run time: " << ((std::clock() - start)/(double)CLOCKS_PER_SEC) << endl;
    outfile.close();
    
    
    outfile.open ("FINCSspectral_inclusion.txt");
    outfile << setw(5) << setprecision(3) << GGM::Inclusion;
    outfile.close();
    
    // Save the inclusion probabilities
    outfile.open ((DataFile_str + ".grapho").c_str());
    outfile << setw(5) << setprecision(3) << GGM::Inclusion;
    outfile.close();


    std::set<double,tolcomp>::reverse_iterator rit;
    outfile.open ("FINCSspectral_logscores.txt");
    rit = DiscoveredScores.rbegin();
    for(ushort i=0;i<min(1000,(int)DiscoveredScores.size());i++)
    {
        outfile << setprecision(10) << *rit << endl;
        rit++;
    }
    outfile.close();
    
    outfile.open ("FINCSspectral_bestgraphs.txt");
    map<double, DecomposableGraph>::reverse_iterator mit = SavedModels.ModelMap.rbegin();
    int upper = min(1000,(int)SavedModels.TopModels);
    for(ushort i = 0; i < upper; i++)
    {
        outfile << setprecision(0) << mit->second.GetIncidence() << endl;
        mit++;
    }
    outfile.close();
    
    return 0;
}

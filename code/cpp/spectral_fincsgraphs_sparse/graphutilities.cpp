#include "graphutilities.h"


NodeSet SetTakeAway(NodeSet larger, NodeSet smaller)
{
  NodeSet Diff = larger;
  std::set<ushort>::iterator it1;
  for(it1 = smaller.begin(); it1 != smaller.end(); it1++)
    {
      Diff.erase(*it1);
    }
  return Diff;
}


NodeSet Intersection(NodeSet a, NodeSet b)
{
  NodeSet c; c.clear();
  if(!(a.empty()) && !(b.empty()))
    {
      std::set<ushort>::iterator it1, it2;
      for(it1 = a.begin(); it1 != a.end(); it1++)
	{
	  it2 = b.find(*it1);
	  if(it2 != b.end()) {c.insert(*it1);}
	}
    }
  return c;
}


bool IsSubset(NodeSet smaller, NodeSet larger)
// Naive subset checking -- scans the first set and
// determines if each member is in the larger set
{
  bool test = false;
  NodeSet K = Intersection(smaller,larger);
  if(K == smaller)
    {
      test = true;
    }
//   bool test = true;
//   if(smaller.size() > larger.size()) {test = IsSubset(larger,smaller);}
//   else
//     {
//       std::set<ushort>::iterator it = smaller.begin();
//       while(it != smaller.end() && test==true)
// 	{
// 	  if(larger.find(*it) == larger.end()) {test=false;}
// 	  it++;
// 	}
//     }
  return test;
}


bool IsSubset(ushort a, NodeSet larger)
// Naive subset checking -- scans the first set and
// determines if each member is in the larger set
{
  bool found = false;
  std::set<ushort>::iterator it;
  it = larger.find(a);
  if(it != larger.end()) {found = true;}
  return found;
}


bool CompareByWeightPointer(WeightedNode a, WeightedNode b)
{
  return( *(a.second) < *(b.second) );
}



bool NodeSetCompare(NodeSet lhs, NodeSet rhs)
// Smaller nodesets are ordered earlier.
// For nodesets of equal size, ordering is done by smallest member,
// with ties broken by second smallest member, and so on.
{
  if(lhs.size() != rhs.size())
    {
      return(lhs.size() < rhs.size());
    }
  else
    {
      bool lt = false, gt = false;
      set< ushort >::iterator it1 = lhs.begin(), it2 = rhs.begin();
      while(lt == false && gt == false && it1 != lhs.end())
	{
	  if(*it1 < *it2) {lt = true;}
	  else if(*it1 > *it2) {gt = true;}
	  it1++;
	  it2++;
	}
      return lt;
    }
}

void NodeSetPrint(NodeSet a)
{
  cout << "{";
  for(set<ushort>::iterator set_it = a.begin(); set_it != a.end(); set_it++)
    {
      cout << *set_it << " ";
    }
  cout << "}";
}


bool lesstol(double a, double b)
{
  return( a < b && b-a > TOL);
}


Details of the algorithm are in Scott and Carvalho, Feature-inclusion stochastic search for Gaussian graphical models, Journal of Computational and Graphical Statistics

This code modifies the original fincs code of scott and Carvalho to do
stochastic search over graphical models of time series.

Dependencies:
  - C++11 compiler (e.g. g++ 4.7 or higher).
  - Newmat matrix library (build instructions included below).
  - Boost compiled with c++11 support.


Instructions for use:

0) Prepare your data.
   
   We assume that the data is a p-dimensional time series with T observations.  
   In the example below the name `var_1_ex1` is an example, you can call it
   whatever you want (though it needs to be the same for different files).
   See the var_1_ex1.* files for examples.

   - Save the (smoothed) periodogram of the time series in two files,
     `var_1_ex1.real` and `var_1_ex1.imag`, one for the real and one for the imaginary
     components.  Both files should contain p columns and T*p lines with the
     entries of the respective matrix entries which should be separated by
     spaces.  Note, each p x p block of this file corresponds to the (smoothed)
     periodogram at a particular frequency.

   - Put the degrees of freedom for each frequency in a file `var_1_ex1.df` the i'th
     line contains the degrees of freedom for the i'th frequency.

   - Create an initial graph in a file, `name.graph`, with a line for each edge
     where the form of a line is 'source_idx dest_idx'.  The indices should be
     in {1,...,p} and the graph is undirected, so only specify edges in one
     directions.  See var_1_ex1.graph for an example.  If the file is empty
     then the empty graph is used as the initial graph.  Note that this file
     can be named anything (with any extension).

1) Create a text file with the following items in it, separated by spaces
(see var_1_ex1.infile for an example):

Prefix name of data files (described above, do not include extensions)
Filename of initial graph 
Number of frequencies in periodogram (T)
Number of observations/rows of X (n)  NOT USED, THE VALUE IS IGNORED
Number of nodes/dimension of series (p)
Prior edge probability (\in (0,1))
Number of iterations
How often to make RMTP move (every nth iteration)
How often to resample an old model
Method to compute the marginal likelihoods
Method to compute prior model probabilities
Number of top models to keep in RAM
g (should typically be 1/n)
degrees of freedom in HIW prior (should be gn)
tau in conventional HIW(delta, tau * I) prior (not implemented)
prior inclusion probability
How often to print information (every nth iteration)


2) Build the Newmat Matrix c++ library.

   - Download Newmat10 from http://www.robertnz.net/download.html into a
     sub-directory `./newmat` by executing the following:
    
     curl -O http://www.robertnz.net/ftp/newmat10.tar.gz
     mkdir newmat
     mv newmat10.tar.gz newmat
     cd newmat
     tar xvzf newmath10.tar.gz

   - Inside the `newmat` directory (which you will be if you ran the above
     commands), edit makefile for your compiler to use a c++11 compatible
     compiler.  For example, if using g++ 4.8, edit the nm_gnu.mak file and
     make sure it is calling the correct compiler and that you add -std=c++11
     to the CFLAGS variable. for example, make CXX = g++-4.8 and add -std=c++11      to CXX flags

   - Build with `make -f nm_gnu.mak libnewmat.a`
   - Return to `spectral_fincs` directory (`cd ..` from the `newmat` directory
     just created).

3) Edit `Makefile` to use the same compiler you used to build Newmat10.

4) Type `make fincs`.

5) Run the `fincs` executable piping your infile into `fincs`, e.g.
   `./fincs < data.infile`.

6) Output will be placed into the following files:

FINCSproperties.txt
FINCSinclusion.txt (edge inclusion probabilities)
FINCSlogscores.txt (best log posterior probabilities found)
FINCSbestgraphs.txt (incidence matrices of the best graphs found, same order as above)

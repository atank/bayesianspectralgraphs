""" drawgraph.py
    
    Draw learned graph from (spectral) fincs.  This can work with a sigle
    graph, or the FINCS*bestgraph.txt file can be passed and an average over
    the best graphs can be used as edge weights.

    Usage:
      drawconngraph.py best <indir> <outdir>
      drawconngraph.py soft <adjmatfile> <labelfile> <outfile>
      drawconngraph.py (-h | --help)

    Options:
      -h --help         Show this screen.

    The soft mode requires the FINCS*bestgraph.txt file to exist as it has
    multiple learned graphs in it.
"""

from __future__ import division

import os
import glob

import numpy as np
import matplotlib.pyplot as plt

import mne

subject_data = "~/work/data/kc_meg_data/raw_50locs"


def viz_connections(A, node_labels, dataname, outfn, n_lines=None,
                    colorbar=False, colormap="hot_r", vmax=None, vmin=None):

    subjects_dir = os.path.expanduser(subject_data)
    # Get labels for FreeSurfer 'aparc' cortical parcellation with 34
    # labels/hemi
    labels = mne.read_labels_from_annot('fsaverage', parc='aparc.a2009s',
                                        subjects_dir=subjects_dir,
                                        surf_name='inflated')
    labels = [l for l in labels if l.name in node_labels]
    label_colors = [label.color for label in labels]

    # First, we reorder the labels based on their location in the left hemi
    label_names = [label.name for label in labels]
    lh_labels = [name for name in label_names if name.endswith('lh')]
    rh_labels = [name for name in label_names if name.endswith('rh')]

    # Get the y-location of the label
    label_ypos = list()
    for name in lh_labels:
        idx = label_names.index(name)
        ypos = np.mean(labels[idx].pos[:, 1])
        label_ypos.append(ypos)
    
    # Reorder the labels based on their location
    lh_labels = [label for (yp, label) in sorted(zip(label_ypos, lh_labels))]
    
    # For the right hemi, we do it explicitly because we threw away regions and
    # so don't have perfect symmetry anymore.
    #rh_labels = [label[:-2] + 'rh' for label in lh_labels]
    rh_labels = [label for (yp, label) in sorted(zip(label_ypos, rh_labels))]
    
    # Save the plot order and create a circular layout
    node_order = list()
    node_order.extend(lh_labels[::-1])  # reverse the order
    node_order.extend(rh_labels)
    
    node_angles = mne.viz.circular_layout(label_names, node_order, start_pos=90,
                                  group_boundaries=[0, len(label_names) / 2])

    if n_lines is None:
        n_lines = np.sum(A).astype('int')

    # Plot the graph using node colors from the FreeSurfer parcellation. We only
    # show the 300 strongest connections.
    mne.viz.plot_connectivity_circle(A, label_names, n_lines=n_lines,
                             node_angles=node_angles, node_colors=label_colors,
                             #title='%s: Region Connectivity' % dataname,
                             title=None, 
                             facecolor='w', textcolor='w', padding=12,
                             colormap=colormap, fontsize_title=16,
                             colorbar=colorbar, vmax=vmax, vmin=vmin)

    plt.savefig(outfn)
    plt.close()



if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    if args['best']:
        indir = args['<indir>']
        outdir = args['<outdir>']

        try:
            os.makedirs(outdir)
        except OSError:
            pass

        adjmatfiles = glob.glob(os.path.join(indir, "*.bestgraph"))
        basenames = [os.path.basename(f) for f in adjmatfiles]
        basenames = [os.path.splitext(f)[0] for f in basenames]
        conds = list(set([n.split('_')[-1] for n in basenames]))
        basepref = "_".join(basenames[0].split('_')[:-1]) + "_"

        for fi, (basename, cond) in enumerate(zip(basenames, conds)):

            labelfile = os.path.join(indir, basename + ".labels")
            with open(labelfile, 'r') as f:
                node_labels = f.readlines()
                node_labels = [l.strip() for l in node_labels]
                node_labels = [l.encode('ascii', 'ignore') for l in node_labels]
            
            adjmatfile = os.path.join(indir, basename + ".bestgraph")
            A = np.loadtxt(adjmatfile)

            outfn = os.path.join(outdir, cond + ".pdf")
        
            viz_connections(A, node_labels, cond, outfn)
        
        # Make difference graphs
        cond_var = list(set([c[0] for c in conds]))
        #cond_att = list(set([c[1:] for c in conds]))
        for cv in cond_var:
            # This assumes two attention states
            std_ = cv + 'Std'
            ron_ = cv + 'Ron'

            labelfile = os.path.join(indir, basepref + std_ + ".labels")
            with open(labelfile, 'r') as f:
                node_labels = f.readlines()
                node_labels = [l.strip() for l in node_labels]
                node_labels = [l.encode('ascii', 'ignore') for l in node_labels]

            adjmat_std = os.path.join(indir, basepref + std_ + ".bestgraph")
            A_std = np.loadtxt(adjmat_std)
            adjmat_ron = os.path.join(indir, basepref + ron_ + ".bestgraph")
            A_ron = np.loadtxt(adjmat_ron)

            inter = np.logical_and(A_std, A_ron)
            D = (A_std - inter) - (A_ron - inter)
            D[D > 1.] = 1.  # In standard but not switching
            D[D < -1.] = -1.
            D[D == -1.] = 0.75  # In switching but not standard

            outfn = os.path.join(outdir, cv + "_diff.pdf")
            viz_connections(D, node_labels, cv, outfn,
                    colorbar=False)

            outfn = os.path.join(outdir, cv + "_inter.pdf")
            viz_connections(inter.astype('int'), node_labels, cv, outfn,
                    colorbar=False, vmax=2., vmin=0., colormap='gray_r')

    elif args['soft']:
        raise RuntimeError("not implemented")
    #    
    #    Alst = list()
    #    with open(adjmatfile, 'r') as f:
    #        lines = f.readlines()
    #        a_lines = list()
    #        p = 0
    #        for li, l in enumerate(lines):
    #            if l == "\n":
    #                tmp = np.fromstring(" ".join(a_lines), dtype=np.uint, sep=' ')
    #                tmp = tmp.reshape((p, p))
    #                Alst.append(tmp)    
    #                a_lines = list()
    #                p = 0
    #            else:
    #                p += 1
    #                a_lines.append(l.strip())

    #    p = Alst[0].shape[0]
    #    A = np.zeros((p, p))
    #    for i in xrange(len(Alst)):
    #        A += Alst[i]
    #    A /= len(Alst)
    #    A *= A > thresh

    else:
        raise RuntimeError('Unknown command')



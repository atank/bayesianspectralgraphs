""" filter_regions.py
    
    Filter MEG data and only keep regions that seem to be active.  Processes
    all subject files in given directory.  Writes a csv file per time series to
    a specified directory.

    Usage:
      filter_regions.py <mode> <rawmegdir> <outdir>
      filter_regions.py (-h | --help)
    
    Options:
      -h, --help    Show this screen.

    Modes:
      variance      Keep locations with sufficiently large variance (i.e.
                    aren't just noise).

"""

from __future__ import division

import os
import sys
import glob

import numpy as np
import scipy.io as sio

import itertools

import pandas as pd


def filter_variance():
    """ Move below into this function at some point.
    """
    pass


if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    mode = args['<mode>']
    rawmegdir = args['<rawmegdir>']
    outdir = args['<outdir>']

    try:
        os.makedirs(outdir)
    except OSError:
        pass

    # Indices of regions that seem to be signal over all subjects, conditions
    # and trials.
    keep_inds = list()

    meg_files = glob.glob(os.path.join(rawmegdir, "*.mat"))

    print "Determining signal regions."
    for mi, mf in enumerate(meg_files):

        meg = sio.loadmat(mf)
        regions = [l.strip() for l in meg['labelNames']]
        conditions = meg['conditions']
        time = meg['t'].squeeze()
        data = meg['data']

        nloc, T, nconds, ntrials = data.shape

        print "  Processing subject %d: %d trials" % (mi, ntrials)

        for ci in xrange(nconds):
            for tr in xrange(ntrials):
                y = np.ascontiguousarray(data[:,:,ci,tr])

                y_mc = y - np.mean(y, axis=1)[:,np.newaxis]
                
                y_var = np.var(y_mc, axis=1)
                var_q = np.percentile(y_var, 11)  # Maps to 50 components for all series

                keep_inds.append(np.where(y_var > var_q)[0])
            

    # Take intersection of all keep_inds for each time series
    I = reduce(lambda l, r: np.intersect1d(l, r), keep_inds)
    print "Found %d common regions" % (I.shape[0],)

    # Go through series again and extract the regions we want to keep
    print "Saving series to files."
    for mi, mf in enumerate(meg_files):
        
        meg = sio.loadmat(mf)
        regions = [l.strip() for l in meg['labelNames']]
        conditions = meg['conditions']
        time = meg['t'].squeeze()
        data = meg['data']

        nloc, T, nconds, ntrials = data.shape

        print "  Saving subject %d" % mi

        dims = "Labels (%d) x Time (%d) x Conds (%d) x Trials (variable)" % (I.shape[0], T, nconds)
        data_new = data[I, :, :, :]
        regions_keep = [regions[i] for i in I]

        out_ext = "mat"  # "csv"
        outstr = "meg_subj_%d.%s" % (mi, out_ext)
        outfile = os.path.join(outdir, outstr)

        mdict = {'labelNames': regions_keep, 'conditions': conditions,
                't': time, 'data': data_new, 'dimensions': dims}
        sio.savemat(outfile, mdict)

        # Old version that was writing series to data frames
        #for ci in xrange(nconds):
        #    for tr in xrange(ntrials):

        #        y = np.ascontiguousarray(data[:,:,ci,tr])

        #        y_mc = y - np.mean(y, axis=1)[:,np.newaxis]
        #        y_mc = y_mc[I,:]
        #        regions_keep = [regions[i] for i in I]

        #        out_ext = ".mat"  # "csv"
        #        outstr = "meg_subj_%d_cond_%s_trial_%d.%s" % (mi, conditions[ci], tr, out_ext)
        #        outfile = os.path.join(outdir, outstr)

        #        df = pd.DataFrame(y_mc.T, columns=regions_keep)
        #        df.to_csv(outfile, index_label=False)

""" make_jobfile.py

    Create jobfile to be used with Grid Engine.

    Usage:
      make_jobfile.py <fincspath> <datadir> <logdir>

"""

import os
import glob


if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    fincspath = os.path.expanduser(args['<fincspath>'])
    datadir = os.path.expanduser(args['<datadir>'])
    logdir = os.path.expanduser(args['<logdir>'])

    exp_names = glob.glob(os.path.join(datadir, "*.infile"))

    try:
        os.makedirs(logdir)
    except OSError:
        pass

    with open("meg.job", 'w') as f:
        for en in exp_names:
            en_nopathext = os.path.splitext(en)[0]
            en_nopathext = os.path.split(en_nopathext)[-1]
            logfile = os.path.join(logdir, "%s.log" % en_nopathext)
            f.write("%s < %s &> %s\n" % (os.path.join(fincspath, "fincs"), en, logfile))

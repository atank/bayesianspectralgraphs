""" process_meg.py

    Process raw MEG data into format for spectral_fincs code.  This code also
    writes out the setup files for spectral fincs.

    Make sure to run this from the meg/ directory or use absolute paths.

    Usage:
      process_meg_holdsubj.py <rawmegdir> <outdir> [--subjholdout=<subj>]
      process_meg_holdsubj.py (-h | --help)


    Options:
      -h --help                 Show this screen.
      --subjholdout=<subj>      Index of subject to hold out {0,...,9} [default: -1]

"""

from __future__ import division

import os
import sys
import glob

import numpy as np
from numpy.fft import fft

import scipy.io as sio

# Ideally these would be optional arguments to the script
niter = 1000  # 100000
rmtp = 1000
resample = 20
marg_method = 1
prior_method = 1
top_models = 1000
df_old = 1  # This isn't used as there's a df file
tau = 1.
prior_inclusion = 0.5
print_it = 100


if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    rawmegdir = os.path.expanduser(args['<rawmegdir>'])
    outdir = os.path.expanduser(args['<outdir>'])
    holdsubj = int(args['--subjholdout'])

    try:
        os.makedirs(outdir)
    except OSError:
        pass

    meg_files = glob.glob(os.path.join(rawmegdir, "*.mat"))

    # Open a file to get the conditions to set up data structures
    meg = sio.loadmat(meg_files[0])
    conditions = meg['conditions']
    data = meg['data']
    nloc, T, nconds, _ = data.shape

    pg_dict = dict()
    df_dict = dict()
    pg_hold_dict = dict()
    df_hold_dict = dict()
    for cond in conditions:
        pg_dict[cond] = np.zeros((nloc, nloc, T), dtype='complex128')
        df_dict[cond] = 0
        pg_hold_dict[cond] = np.zeros((nloc, nloc, T), dtype='complex128')
        df_hold_dict[cond] = 0

    # Write initial empty graph
    with open(os.path.join(outdir, "empty.graph"), 'w') as f:
        f.write("")

    print "Processing series"
    for mi, mf in enumerate(meg_files):

        if mi == holdsubj:
            print "  Subject %d (test subject)" % mi
        else:
            print "  Subject %d" % mi

        meg = sio.loadmat(mf)
        regions = [l.strip() for l in meg['labelNames']]
        #conditions = meg['conditions']
        time = meg['t'].squeeze()
        data = meg['data']

        nloc, T, nconds, ntrials = data.shape

        for ci in xrange(nconds):
            cond = conditions[ci]

            if mi == holdsubj:
                pg_cond = pg_hold_dict[cond]
            else:
                pg_cond = pg_dict[cond]

            for tr in xrange(ntrials):

                y = np.ascontiguousarray(data[:,:,ci,tr])
                y_mc = y - np.mean(y, axis=1)[:,np.newaxis]
                y_std = np.std(y_mc, axis=1)
                y_z = y_mc / y_std[:,np.newaxis]

                # Compute periodogram
                d = fft(y_z, axis=1)
                for t in xrange(T):
                    pg_cond[:,:,t] += np.outer(d[:,t], d[:,t].conj())
                
                if mi == holdsubj:
                    df_hold_dict[cond] += 1
                else:
                    df_dict[cond] += 1

    # Set up fincs stuff, like config file and whatnot
    
    print "Writing files"
    for ci, cond in enumerate(conditions):
        print "Processing conditions: %d / %d" % (ci, len(conditions))
        outprefix = "meg_cond_%s" % cond
        outname = os.path.join(outdir, outprefix)
        testname = os.path.join(outdir, outprefix)
        I = pg_dict[cond]
        df = df_dict[cond]
        df_test = df_hold_dict[cond]

        # Write training data
        # Save real and imaginary components separately
        Ir = I.real
        Ii = I.imag

        fr = open(outname + ".real", 'w')
        fi = open(outname + ".imag", 'w')
        for t in xrange(T):
            np.savetxt(fr, np.ascontiguousarray(Ir[:,:,t]))
            np.savetxt(fi, np.ascontiguousarray(Ii[:,:,t]))
        fr.close()
        fi.close()

        # Write testing data
        I = pg_hold_dict[cond]
        Ir = I.real
        Ii = I.imag
        fr = open(testname + ".real.test", 'w')
        fi = open(testname + ".imag.test", 'w')
        for t in xrange(T):
            np.savetxt(fr, np.ascontiguousarray(Ir[:,:,t]))
            np.savetxt(fi, np.ascontiguousarray(Ii[:,:,t]))
        fr.close()
        fi.close()

        # Save degrees of freedom vector (train and test)
        with open(outname + ".df", 'w') as f:
            for t in xrange(T):
                f.write("%d\n" % df)

        with open(testname + ".df.test", 'w') as f:
            for t in xrange(T):
                f.write("%d\n" % df_test)

        # fincs config file
        with open(outname + ".infile", 'w') as f:
            outstr = "%s %s %d %d %d %d %d %d %d %d %d %f %f %f %f %d" % \
                     (outname, "empty.graph", T, T, nloc, niter, rmtp, resample,
                             marg_method, prior_method, top_models, 4./df,
                             df_old, tau, prior_inclusion, print_it)
            f.write(outstr) 

        # Write region labels
        with open(outname + ".labels", 'w') as f:
            for r in regions:
                f.write("%s\n" % r)

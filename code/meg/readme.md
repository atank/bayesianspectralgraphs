
MEG Experiments
===============

This assumes that the raw data is stored in `/data/` somewhere.

Filter out noise components
---------------------------

1. Run `spectral_graph_filter.py` pointed at the raw data and somewhere to
   store the small resultant data.


Cluster workflow
------------------------------

1. If want to learn the graph on all of the data:
   `process_meg.py` pointed at the small data resulting from filtering out the
   noise components above.
   Otherwise, if want test data run the following for each subject, X:
   `process_meg_traintest.py` <rawdata> <outdir> --subjholdout=X`
   Note that the outdir should probably have X in it somewhere so that the jobs
   can be run simultaneously.
2. `make_jobs.py`
3. Find out how many jobs to run: `wc -l meg.job`
4. Get the `qsub` command because who can remember them: `cat qsub_meg.sh`
5. Run the qsub command printed from step above.


Predictive log-lik (two-way)
----------------------------

1. `testcond_predll_confusion.py` which generates a csv file like:
   truelabel,predlabel,log-lik
   
   where `truelabel` and `predlabel` have the same modality (pitch/spatial) but
   possibly different attention states.
2. Do something with this.

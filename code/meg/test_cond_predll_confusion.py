""" testcond_predll_confusion.py

    Compute predictive log-marginal-likelihood of held-out data.

    There are two modes:
    * `subj` computes predictive log-probs for each series corresponding to a test
      subject using a learned graph and the training periodograms.
    * `stat` aggregates the results from the `subj` mode over multiple hold out
            subjects (the results should be <indir> which corresponds to
            <outdir from `subj` version) and computes performance statistic.

    Usage:
      testcond_predll_confusion.py subj <datadir> <outdir> <subjname>
      testcond_predll_confusion.py stat <indir> <outfile>
      testcond_predll_confusion.py (-h | --help)

    Options:
      -h --help     Show this screen
"""

from __future__ import division

import os
import sys
import glob
import cPickle as pkl

import numpy as np

import rpy2.robjects as robjects
import rpy2.robjects.numpy2ri


if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    if args['subj']:

        datadir = args['<datadir>']
        outdir = args['<outdir>']
        subjname = args['<subjname>']

        try:
            os.makedirs(outdir)
        except OSError:
            pass

        r = robjects.r
        rpy2.robjects.numpy2ri.activate()
        r['source']("../R/held_out_test.R")
        cnw_lik = r['CNW_full_likelihood']

        cond_files = glob.glob(os.path.join(datadir, "*.real"))
        cond_prefix = [os.path.basename(c) for c in cond_files]
        cond_prefix = [os.path.splitext(c)[0] for c in cond_prefix]
        conds = [c.split('_')[-1] for c in cond_prefix]


        cond_dict = {c: cp for (c, cp) in zip(conds, cond_prefix)}

        cond_tr_dat = dict()
        cond_te_dat = dict()

        for ci, (cond, cond_p) in enumerate(cond_dict.iteritems()):
            
            print "condition: %s, (%d/%d)" % (cond, ci+1, len(cond_dict)),
            inprefix = os.path.join(datadir, cond_p)

            # Load training files
            print "  Loading training...",
            sys.stdout.flush()
            
            try:
                with open(inprefix + ".pkl", 'rb') as f:
                    d = pkl.load(f)
                    G = d['G']
                    I_tr = d['I_tr']
                    df_tr = d['df_tr']
                    p = G.shape[0]
                    T = I_tr.shape[2]
            except IOError:

                G = np.loadtxt(inprefix + ".bestgraph")
                p = G.shape[0]

                tmp_r = np.loadtxt(inprefix + ".real")
                tmp_i = np.loadtxt(inprefix + ".imag")
                T = tmp_r.shape[0] // p
                
                I_r = np.empty((p,p,T))
                I_i = np.empty((p,p,T))

                for t in xrange(T):
                    I_r[:,:,t] = tmp_r[(t*p):((t+1)*p),:]
                    I_i[:,:,t] = tmp_i[(t*p):((t+1)*p),:]

                I_tr = I_r + 1.0j*I_i

                df_tr = np.loadtxt(inprefix + ".df")[0]

                d = {'G': G, 'I_tr': I_tr, 'df_tr': df_tr}
                with open(inprefix + ".pkl", 'wb') as f:
                    pkl.dump(d, f)

            cond_tr_dat[cond] = (G, I_tr, df_tr)

            print "  Loading testing..."
            sys.stdout.flush()

            try:
                with open(inprefix + ".pkl.test", 'rb') as f:
                    d = pkl.load(f)
                    I_te = d['I_te']
                    df_te = d['df_te']
            except IOError:

                tmp_r = np.loadtxt(inprefix + ".real.test")
                tmp_i = np.loadtxt(inprefix + ".imag.test")
                T = tmp_r.shape[0] // p
                
                I_r = np.empty((p,p,T))
                I_i = np.empty((p,p,T))

                for t in xrange(T):
                    I_r[:,:,t] = tmp_r[(t*p):((t+1)*p),:]
                    I_i[:,:,t] = tmp_i[(t*p):((t+1)*p),:]

                I_te = I_r + 1.0j*I_i

                df_te = np.loadtxt(inprefix + ".df")[0]

                d = {'I_te': I_te, 'df_te': df_te}
                with open(inprefix + ".pkl.test", 'wb') as f:
                    pkl.dump(d, f)

            cond_te_dat[cond] = (I_te, df_te)


        # For each condition in the test set, see if it's better predicted by
        # the standard or switching graph.  For example, see if LStd is better
        # predicted (higher marginal-likelihood) by the learned LStd or LRon.

        print "Computing predictive..."

        pred_ll_l = list()

        for i, cond_te in enumerate(conds):
            I_te, df_te = cond_te_dat[cond_te]

            cond_var = cond_te[0]
            cond_att = cond_te[1:]

            conds_train = [cc for cc in conds if cc[0] == cond_var]

            pred_ll_d = dict()
            for j, cond_tr in enumerate(conds_train):
                G, I_tr, df_tr = cond_tr_dat[cond_tr]
                G_r = r['matrix'](G, nrow=p)
                G_graph = r['as'](G_r, "graphNEL")

                llik = cnw_lik(df_tr, I_tr, G_graph, I_te, df_te, T, p, False, 1.)
                pred_ll_d[cond_tr] = llik[0]

            pred_ll_l.append(pred_ll_d)   


        with open(os.path.join(outdir, "%s.csv" % subjname), 'w') as f:
            for i, cond_te in enumerate(conds):
                for k, v in pred_ll_l[i].iteritems():
                    f.write("%s,%s,%f\n" % (cond_te, k, v))

    elif args['stat']:
        raise RuntimeError("not implemented!")

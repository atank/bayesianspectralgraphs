from __future__ import division

import numpy as np 
import scipy as sp
import scipy.special

from scipy.special import gammaln, betaln

#####k is number of edges in graph, n is the total number of pairs of edges, alpha and beta hyperparameters. beta > alpha
####favors sparse graphs, alpha > beta favors dense. alpha = beta = 1 is generally default 
def log_betabinom(k, n, alpha, beta):
    return ((gammaln(n+1) - (gammaln(k+1) + gammaln(n-k+1)))
            + betaln(k+alpha, n-k+beta) - betaln(alpha, beta))


def log_beta_bernoulli_def(k,n):
    return (gammaln(n-k + 1) + gammaln(k + 1) - gammaln(n+1) - np.log(n + 1))

# G is a symmetric matrix with ZEROS ON THE DIAGONAL where there is a one at
# (i,j) and (j,i) if there is an edge between these nodes in the graph
def graph_logprior(G, alpha=1,beta=1):
    k = G.sum()/2
    p = G.shape[1]
    n = (p**2 - p)/2
    return(log_beta_bernoulli_def(k,n))
#return log_betabinom(k,n,1,1)

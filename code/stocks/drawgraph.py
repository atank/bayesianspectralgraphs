""" drawgraph.py
    
    Draw learned graph from (spectral) fincs.  This can work with a sigle
    graph, or the FINCS*bestgraph.txt file can be passed and an average over
    the best graphs can be used as edge weights.

    Usage:
      drawgraph.py best <adjmatfile> <labelfile> <outfile> [--layout=<lay>]
      drawgraph.py soft <adjmatfile> <labelfile> <outfile> [--layout=<lay>]
                   [--threshold=<t>]
      drawgraph.py (-h | --help)

    Options:
      -h --help         Show this screen.
      --layout=<lay>    Layout to use ('spectral', 'spring') [default: spring]
      --threshold=<t>   Edge threshold for soft mode [default: 0.0]

    The soft mode requires the FINCS*bestgraph.txt file to exist as it has
    multiple learned graphs in it.
"""

from __future__ import division


import numpy as np
import networkx as nx

import matplotlib.pyplot as plt


if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    adjmatfile = args['<adjmatfile>']
    labelfile = args['<labelfile>']
    outfile = args['<outfile>']
    layout = args['--layout']
    thresh = float(args['--threshold'])

    with open(labelfile, 'r') as f:
        labs = f.readlines()
        labs = [l.strip() for l in labs]
    
    labels = {i: l for i, l in enumerate(labs)}

    if args['best']:
        A = np.loadtxt(adjmatfile)

    elif args['soft']:
        
        Alst = list()
        with open(adjmatfile, 'r') as f:
            lines = f.readlines()
            a_lines = list()
            p = 0
            for li, l in enumerate(lines):
                if l == "\n":
                    tmp = np.fromstring(" ".join(a_lines), dtype=np.uint, sep=' ')
                    tmp = tmp.reshape((p, p))
                    Alst.append(tmp)    
                    a_lines = list()
                    p = 0
                else:
                    p += 1
                    a_lines.append(l.strip())

        p = Alst[0].shape[0]
        A = np.zeros((p, p))
        for i in xrange(len(Alst)):
            A += Alst[i]
        A /= len(Alst)
        A *= A > thresh

    else:
        raise RuntimeError('Unknown command')

    G = nx.Graph(A)

    # Now actually draw stuff
    if layout == 'spring':
        pos = nx.spring_layout(G)
    elif layout == 'spectral':
        pos = nx.spectral_layout(G)
    else:
        raise RuntimeError('Unknown layout')

    np.random.seed(13)
    nx.draw_networkx_nodes(G, pos, node_color='w', node_size=500)
    if args['best']:
        nx.draw_networkx_edges(G, pos, alpha=0.8)
    elif args['soft']:
        edges = G.edges()
        weights = [5*G[u][v]['weight'] for u, v in edges]
        nx.draw_networkx_edges(G, pos, edges=edges, width=weights)
    nx.draw_networkx_labels(G, pos, labels)

    plt.axis('off')
    plt.savefig(outfile, transparent=True)
    plt.close()

#!/usr/bin/bash

# $1 is path to directory with data file
# $2 is path to metadata file
# $3 is path to directory to write output

python prep_spectral_fincs.py $1 $2 $3 --winw=17
python prep_fincs.py $1 $2 "$3_ind"

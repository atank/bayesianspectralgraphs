""" predll_stocks.py
    
    Compute predictive log-fractional-probability of periodogram for held out
    stock prices.

    Usage:
      predll_stocks.py <retdata> <dataprefix>
"""

from __future__ import division

import os
import sys
import cPickle as pkl

import numpy as np
import pandas as pd
from numpy.fft import fft

import rpy2.robjects as robjects
import rpy2.robjects.numpy2ri as rpyn

# This matches prep_spectral_fincs and is from Songsiri.
ret_mult = 100.


if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    datfile = args['<retdata>']
    dataprefix = args['<dataprefix>']

    prices = pd.read_csv(datfile, index_col=0)
    returns = np.log(prices.pct_change()+1.)
    returns = ret_mult*returns[1:]

    ret_data = returns.values
    T, p = ret_data.shape
    # Make and even-length series for prediction
    if T % 2 != 0:
        ret_data = ret_data[:-1,:]
        T = ret_data.shape[0]
    ret_data -= np.mean(ret_data, axis=0)

    # Load training and testing (smoothed) periodograms.
    with open(dataprefix + "-train.pkl", 'rb') as f:
        Itrain = pkl.load(f)
    with open(dataprefix + "-test.pkl", 'rb') as f:
        Itest = pkl.load(f)
    
    Ttrain = Itrain.shape[2]
    Ttest = Itest.shape[2]
    assert Ttrain == Ttest

    ret_train = ret_data[:Ttrain,:]
    ret_test = ret_data[Ttrain:, :]

    r = robjects.r
    rpyn.activate()
    r['source']("../R/held_out_test.R")
    cnw_lik = r['CNW_full_likelihood']

    G = np.loadtxt(dataprefix + "-train.bestgraph")
    p = G.shape[0]
    df_tr = np.loadtxt(dataprefix + "-train.df")
    df_te = np.loadtxt(dataprefix + "-test.df")[0]

    G_r = r['matrix'](G, nrow=p)
    G_graph = r['as'](G_r, "graphNEL")

    # Full graph and use periodogram
    pg_train = np.zeros((p, p, Ttrain), dtype='complex128')
    pg_test = np.zeros((p, p, Ttest), dtype='complex128')
    d_train = fft(ret_train, axis=0)
    for t in xrange(Ttrain):
        pg_train[:,:,t] = np.outer(d_train[t,:], d_train[t,:].conj())

    d_test = fft(ret_test, axis=0)
    for t in xrange(Ttest):
        pg_test[:,:,t] = np.outer(d_test[t,:], d_test[t,:].conj())
    
    Gfull = np.ones((p, p))
    Gfull -= np.eye(p)
    Gfull_r = r['matrix'](Gfull, nrow=p)
    Gfull_graph = r['as'](Gfull_r, "graphNEL")

    Gempty = np.zeros((p, p))
    Gempty_r = r['matrix'](Gempty, nrow=p)
    Gempty_graph = r['as'](Gempty_r, "graphNEL")

    llik_spectral = cnw_lik(df_tr, Itrain, G_graph, pg_test, df_te, Ttest, p, False, 1.)
    llik_spectral_full = cnw_lik(np.ones(Ttrain), Itrain, Gfull_graph, pg_test, df_te,
                                 Ttrain, p, False, 1.)
    llik_spectral_empty = cnw_lik(np.ones(Ttrain), Itrain, Gempty_graph, pg_test, df_te,
                                 Ttrain, p, False, 1.)

    print "Spectral: %.4f" % rpyn.ri2py(llik_spectral)
    print "Spectral (empty): %.4f" % rpyn.ri2py(llik_spectral_empty)
    print "Spectral (full): %.4f" % rpyn.ri2py(llik_spectral_full)

    # Independent models
    
    # Need predictive log-likihood...

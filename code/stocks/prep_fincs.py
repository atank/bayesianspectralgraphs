""" prep_fincs.py

    Prepare stock index data for fincs.

    Usage:
      prep_fincs.py <datafile> <metafile> <outdir> [--df=<df>]
      prep_fincs.py (-h | --help)


    Options:
      -h --help     Show this screen.
      --df=<df>     Degrees of freedom.

"""

from __future__ import division

import os
import sys
import glob

import cPickle as pkl

import numpy as np
from numpy.fft import fft

import pandas as pd

# Ideally these would be optional arguments to the script
niter = 100000
rmtp = 1000
resample = 20
marg_method = 1
prior_method = 1
top_models = 1000
tau = 1.
prior_inclusion = 0.5
print_it = 100


# This is from the Songsiri paper
ret_mult = 100.


if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    datafile = os.path.expanduser(args['<datafile>'])
    metafile = os.path.expanduser(args['<metafile>'])
    outdir = os.path.expanduser(args['<outdir>'])
    df = args['--df']

    try:
        os.makedirs(outdir)
    except OSError:
        pass

    prices = pd.read_csv(datafile, index_col=0)
    returns = np.log(prices.pct_change()+1.)
    returns = ret_mult*returns[1:]

    ret_data = returns.values
    T, p = ret_data.shape

    # Write spectral_fincs data and config files
    print "Writing spectral_fincs files"
    
    outprefix = "stock_inds_independent"
    outname = os.path.join(outdir, outprefix)


    fr = open(outname + ".data", 'w')
    np.savetxt(fr, np.ascontiguousarray(ret_data))
    fr.close()

    g = 4. / T
    if df is None:
        df = T*g
    else:
        df = float(df)

    # fincs config file
    with open(outname + ".infile", 'w') as f:
        outstr = "%s %d %d %d %d %d %d %d %d %f %f %f %f %d" % \
                 (outname + ".data", T, p, niter, rmtp, resample,
                  marg_method, prior_method, top_models, g, df,
                  tau, prior_inclusion, print_it)
        f.write(outstr) 

    # Write node labels
    with open(metafile, 'r') as f:
        meta_dict = pkl.load(f)

    with open(outname + ".indexnames", 'w') as f:
        for c in returns.columns:
            f.write("%s\n" % (meta_dict[c][0],))

    with open(outname + ".country", 'w') as f:
        for c in returns.columns:
            f.write("%s\n" % (meta_dict[c][1],))

    with open(outname + ".countrycode", 'w') as f:
        for c in returns.columns:
            f.write("%s\n" % (meta_dict[c][2],))

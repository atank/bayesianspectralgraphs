""" prep_spectral_fincs.py

    Prepare stock index data for spectral_fincs.

    Usage:
      prep_spectral_fincs.py <datafile> <metafile> <outdir> [--winw=<ww>] [--testfrac=<tf>]
      prep_spectral_fincs.py (-h | --help)


    Options:
      -h --help         Show this screen.
      --winw=<ww>       Width of Daniel smoother (Must be >= dimensionality).
      --testfrac=<tf>   Fracion of series to use as test set.

"""

from __future__ import division

import os
import sys
import glob

import cPickle as pkl

import numpy as np
from numpy.fft import fft

import pandas as pd

# Ideally these would be optional arguments to the script
niter = 100000
rmtp = 1000
resample = 20
marg_method = 1
prior_method = 1
top_models = 1000
df_old = 1  # This isn't used as there's a df file
tau = 1.
prior_inclusion = 0.5
print_it = 100


# This is from the Songsiri paper
ret_mult = 100.


if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    datafile = os.path.expanduser(args['<datafile>'])
    metafile = os.path.expanduser(args['<metafile>'])
    outdir = os.path.expanduser(args['<outdir>'])
    winw = args['--winw']
    testfrac = args['--testfrac']

    try:
        os.makedirs(outdir)
    except OSError:
        pass

    prices = pd.read_csv(datafile, index_col=0)
    returns = np.log(prices.pct_change()+1.)
    returns = ret_mult*returns[1:]

    ret_data = returns.values
    T, p = ret_data.shape
    # Make and even-length series for prediction
    if T % 2 != 0:
        ret_data = ret_data[:-1,:]
        T = ret_data.shape[0]
    ret_data -= np.mean(ret_data, axis=0)

    if testfrac is None:
        testfrac = 0.
        ntest = 0
        ntrain = T
    else:
        # THIS HAS TO BE SO THAT WE CAN PREDICT WITHOUT DOWNSAMPLING
        testfrac = 0.5
        ntest = int(np.ceil(T * testfrac))
        ntrain = T - ntest

    # Hold out test data
    if ntest > 0:
        ret_train = ret_data[:ntrain,:]
        ret_test = ret_data[-ntest:,:]

    pg = np.zeros((p, p, T), dtype='complex128')
    pg_train = np.zeros((p, p, ntrain), dtype='complex128')
    pg_test = np.zeros((p, p, ntest), dtype='complex128')

    d = fft(ret_data, axis=0)
    for t in xrange(T):
        pg[:,:,t] = np.outer(d[t,:], d[t,:].conj())

    if ntest > 0:
        d_train = fft(ret_train, axis=0)
        for t in xrange(ntrain):
            pg_train[:,:,t] = np.outer(d_train[t,:], d_train[t,:].conj())

        d_test = fft(ret_test, axis=0)
        for t in xrange(ntest):
            pg_test[:,:,t] = np.outer(d_test[t,:], d_test[t,:].conj())

    # Smooth periodogram with Daniel smoother
    if winw is None:
        winw = p
    else:
        winw = int(winw)
    winw_train = np.floor(winw * (1.-testfrac))

    Ismooth = np.zeros_like(pg)
    df = np.zeros(T)
    for t in xrange(T):
        w1 = np.maximum(0, t-winw)
        w2 = np.minimum(T, t+winw) + 1  # Just a numpy thing
        Ismooth[:,:,t] = np.sum(pg[:,:,w1:w2], axis=2)
        df[t] = w2 - w1  # 2*w + 1

    # Smoothing training but NOT test pg
    Ismooth_train = np.zeros_like(pg_train)
    Ismooth_test = None
    df_train = np.zeros(ntrain)
    df_test = np.zeros(ntest)
    if ntest > 0:
        for t in xrange(ntrain):
            w1 = np.maximum(0, t-winw_train)
            w2 = np.minimum(ntrain, t+winw_train) + 1  # Just a numpy thing
            Ismooth_train[:,:,t] = np.sum(pg_train[:,:,w1:w2], axis=2)
            df_train[t] = w2 - w1  # 2*w + 1
        Ismooth_test = pg_test
        for t in xrange(ntest):
            df_test[t] = 1

    # Write spectral_fincs data and config files
    print "Writing spectral_fincs files"
    
    outprefix = "stock_inds_winw-%d" % (winw,)
    outname = os.path.join(outdir, outprefix)

    Ir = Ismooth.real
    Ii = Ismooth.imag

    fr = open(outname + ".real", 'w')
    fi = open(outname + ".imag", 'w')
    for t in xrange(T):
        np.savetxt(fr, np.ascontiguousarray(Ir[:,:,t]))
        np.savetxt(fi, np.ascontiguousarray(Ii[:,:,t]))
    fr.close()
    fi.close()

    # Save degrees of freedom vector
    with open(outname + ".df", 'w') as f:
        for t in xrange(T):
            f.write("%d\n" % df[t])

    # fincs config file
    with open(outname + ".infile", 'w') as f:
        outstr = "%s %s %d %d %d %d %d %d %d %d %d %f %f %f %f %d" % \
                 (outname, "empty.graph", T, T, p, niter, rmtp, resample,
                         marg_method, prior_method, top_models, 4./df[winw],
                         df_old, tau, prior_inclusion, print_it)
        f.write(outstr) 

    # Write node labels
    with open(metafile, 'r') as f:
        meta_dict = pkl.load(f)

    with open(outname + ".indexnames", 'w') as f:
        for c in returns.columns:
            f.write("%s\n" % (meta_dict[c][0],))

    with open(outname + ".country", 'w') as f:
        for c in returns.columns:
            f.write("%s\n" % (meta_dict[c][1],))

    with open(outname + ".countrycode", 'w') as f:
        for c in returns.columns:
            f.write("%s\n" % (meta_dict[c][2],))

    ## Write prediction fincs files
    if ntest > 0:
        outprefix = "stock_inds_pred_winw-%d" % (winw_train,)
        outname = os.path.join(outdir, outprefix)

        for (Ism_lab, Ism, df_, T_) in [('train', Ismooth_train, df_train, ntrain),
                                    ('test', Ismooth_test, df_test, ntest)]:
            Ir = Ism.real
            Ii = Ism.imag

            fr = open(outname + "-%s.real" % Ism_lab, 'w')
            fi = open(outname + "-%s.imag" % Ism_lab, 'w')
            for t in xrange(T_):
                np.savetxt(fr, np.ascontiguousarray(Ir[:,:,t]))
                np.savetxt(fi, np.ascontiguousarray(Ii[:,:,t]))
            fr.close()
            fi.close()

            # Save a pkl file too for easy loading in prediction code
            with open(outname + "-%s.pkl" % Ism_lab, 'wb') as f:
                pkl.dump(Ism, f)

            # Save degrees of freedom vector
            with open(outname + "-%s.df" % Ism_lab, 'w') as f:
                for t in xrange(T_):
                    f.write("%d\n" % df_[t])

        # fincs config file - need the -train b/c of fincs frailty
        with open(outname + "-train.infile", 'w') as f:
            outstr = "%s %s %d %d %d %d %d %d %d %d %d %f %f %f %f %d" % \
                     (outname + "-train", "empty.graph", ntrain, ntrain, p, niter, rmtp, resample,
                             marg_method, prior_method, top_models,
                             4./df_train[winw_train], df_old, tau,
                             prior_inclusion, print_it)
            f.write(outstr) 

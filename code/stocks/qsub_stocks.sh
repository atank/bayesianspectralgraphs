#!/bin/bash

# NOTE: Make sure to run make_jobfile.py FIRST.

# Submit this job like `qsub -t 1-MAX -V -b yes -cwd ./qsub_meg.sh`
# You can get MAX with something like `wc -l stocks.job`.

CMD=$(awk "NR==$SGE_TASK_ID" stocks.job)
eval $CMD

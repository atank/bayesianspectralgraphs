\section{A BAYESIAN APPROACH}\label{sec:method}
%Our approach to inferring the PGM combines existing Whittle likelihood based methods \cite{Bach:2004, Jung:2014} with the hyper Markov framework to Bayesian graphical modeling \cite{jones:2005, Giudici:1999}. In particular, we place a novel hyper Markov prior, the complex hyper inverse Wishart, independently on each spectral density matrix. Structure is shared across frequencies by using the same graph for each prior distribution. Finally, since the scale of the spectral density varies dramatically across frequencies and there is little prior information about this scale we adapt the fractional prior framework of \cite{Carvalho:2009}, originally developed for consistent model selection across graphs in the iid case, to our spectral formulation. By integrating out this prior over spectral densities we obtain a marginal likelihood of the time series given a graph, easily allowing us to leverage state of the art computational methods for Bayesian graphical inference. 
There are two standard approaches to Bayesian inference in graphical models: (1) placing a prior that jointly specifies the graph structure and associated parameters or (2) placing a prior on graph structures and then a prior on parameters given a graph; both rely on specifying a likelihood model.  We opt for the second approach and describe the various components in this section.  At a high level, our methods combine existing Whittle likelihood based methods \cite{Bach:2004, Jung:2014} with the hyper Markov framework to Bayesian graphical modeling \cite{jones:2005, Giudici:1999}.  In the context of our TGMs, we introduce a conjugate \emph{hyper complex inverse Wishart} prior on graph-constrained spectral density matrices.  By integrating out the spectral density matrices, we obtain a marginal likelihood of the time series given the graph structure, $G$, allowing us to straightforwardly leverage state-of-the-art computational methods for i.i.d. Bayesian structure learning. 

\subsection{Whittle likelihood}
Let $\BF{X} = [X(1), \ldots, X(T)]$, with $x(t) \in \mathbb{R}^p$ a realization of a $p$-dimensional stationary Gaussian time series observed at $T$ time points, and $\BF{X}_{1:N} = \{\BF{X}^1, \ldots, \BF{X}^N\}$ be the collection of $N$ independent realizations.  We move to the frequency domain by transforming each $\BF{X}^i$ using a discrete Fourier transform.  Let $d_{nk} \in \mathbb{C}^p$ denote the discrete Fourier coefficient associated with the $n$th time series at frequency $\lambda_k = \frac{2 \pi k}{T}$:
%
\begin{align}
d_{nk} = \frac{1}{T} \sum_{t = 0}^{T - 1} x_n(t) e^{- i \lambda_k t }.
\end{align}
%
The Whittle approximation \cite{Whittle:1953} assumes the Fourier coefficients are independent \emph{complex normal random variables} with mean zero and covariance given by the corresponding spectral density matrix $S_k = S(\lambda_k)$:
%
\begin{align}
	d_{nk} \sim \mathcal{N}_c(0,S_k) \quad k=0,\dots,T-1,
\end{align}
%
such that the likelihood of $\BF{X}_{1:N}$ is approximated as
\begin{align} \label{eq:whittle}
p(\BF{X}_{1:N}|S_{0:T-1}) \approx \prod_{n = 1}^{N} \prod_{k = 0}^{T - 1} \frac{1}{\pi^p |S_k|} e^{-d_{nk}^* S^{-1}_k d_{nk}},
\end{align}
%
where $\frac{1}{\pi^p |S|} e^{-z^* S^{-1} z}$ is the density of a complex normal distribution, $\mathcal{N}_c(0,S)$, with $S \in \mathbb{C}^{p\times p}$ and Hermitian positive definite.  See the Supplement.  The Whittle approximation holds asymptotically with large $T$ \cite{Brockwell:1991,Brillinger:2001,Whittle:1953}.  This approximation has been used in the Bayesian context in \cite{Rosen:2007,Krafty:2015} 
%\cite{Rosen:2007,Krafty:2015,Combrexelle:15}.

Recall that conditional independencies are encoded in the off diagonal elements of $S_k^{-1}$. If time series $X_i(t)$ and $X_j(t)$ are conditionally independent, then the Whittle approximation says that as $T$ gets large the $i$th and $j$th elements of the Fourier coefficients $d_{nk}$ are conditional independent across all frequencies. Thus, if $G$ is decomposable, Eq.~\eqref{eq:whittle} can be rewritten as
\begin{align}
	\label{eq:whittlegraph}
p(\BF{X}_{1:N}| G, S_{0:(T - 1)})& \approx \\
\,\,\,\,\,\,\,\,\,\, \prod_{k = 0}^{T - 1} &\frac{\prod_{C \in \mathcal{C}}  \frac{1}{\pi^{N |C|} |S_{kC}|^N} e^{-\text{tr} P_{kC} S_{kC}^{-1}}}{ \prod_{S \in \mathcal{S}}  \frac{1}{\pi^{N |S| }|S_{kS}|^N} e^{-\text{tr} P_{kS} S_{kS}^{-1}}} \nonumber
\end{align}
where 
%
\begin{align}
	\label{eq:periodogram}
	P_k = \sum_{n = 1}^{N} d_{nk} d_{nk}^*
\end{align}
is the aggregate \emph{periodogram} over the $N$ time series at frequency $\frac{2 \pi k}{T}$. For $A \subset V$, $S_{kA}$ and $P_{kA}$ are the restriction of both matrices to the elements in $A$ and $|A|$ denotes the cardinality of the set $A$.

\subsection{Hyper complex inverse Wishart prior on graph-constrained spectral density matrices}
%\subsection{HYPER COMPLEX INVERSE WISHART PRIOR ON GRAPH-CONSTRAINED SPECTRAL DENSITY MATRICES}
We seek a prior for the spectral density matrices, $S_k$, whose inverses each have zeros dictated by a graph $G$. Recall that these $S_k$ matrices are complex-valued and restricted to be Hermitian positive definite.  As discussed in Sec.~\ref{sec:hyperMarkov}, the hyper inverse Wishart distribution serves as a prior for real-valued, positive-definite matrices with pre-specified zeros in the inverse, and is a conjugate prior for the covariance of a zero-mean GGM.  Motivated by the connection between GGMs and our TGMs, and the analogous structure of our TGM-based Whittle likelihood of Eq.~\eqref{eq:whittlegraph} to that of a GGM with $N$ i.i.d. observations, we propose a novel \emph{hyper complex inverse Wishart} prior with density function
\begin{align} 
p(\Sigma|\delta,W,G ) &= \propto {\BF 1}_{\Sigma \in M^{+}(G)} |\Sigma|^{-(\delta + 2 p)} e^{- \text{tr} W \Sigma^{-1}}
\end{align}
for \emph{degrees of freedom} $\delta > 0$, \emph{scale matrix} $W \in \mathbb{C}^{p \times p} $ positive definite and Hermitian, and graph $G$.  We have used an analogous parameterization to that of the hyper inverse Wishart \cite{Dawid:1993}. Here, $\Sigma \in M^{+}(G)$ denotes that $\Sigma$ is in the set of all Hermitian positive-definite matrices with $\left(\Sigma^{-1}\right)_{ij}=0$ for all $(i,j) \notin E$.  When $G$ is decomposable, the normalization constant is available and the density decomposes over cliques and separators:
\begin{align}
p(\Sigma|\delta,W,G ) &= \frac{\prod_{C \in \mathcal{C}} \mbox{IW}_c(\Sigma_C|\delta,W_C)}{\prod_{S \in \mathcal{S}} \mbox{IW}_c(\Sigma_C|\delta,W_C)}\\
&= \frac{\prod_{C \in \mathcal{C}} B(W_C, \delta) |\Sigma_{C}|^{-(\delta + 2 |C|)} e^{-\text{tr} W_C \Sigma_{C}^{-1}}} {\prod_{S \in \mathcal{S}}B(W_S, \delta) |\Sigma_{S}|^{-(\delta + 2 |S|)} e^{-\text{tr} W_S \Sigma_{S}^{-1}}},
\end{align}
%
where $\mbox{IW}_c$ denotes the complex inverse Wishart \cite{Brillinger:2001} detailed in the Supplement with normalizer 
%
\begin{align}
B(W,\delta) = \frac{|W|^{\delta + p}}{\pi^{\frac{p(p - 1)}{2}} \prod_{j = 1}^{p} (\delta + p - j)!}.
\end{align}
%
% See the Supplement for more details on the complex inverse Wishart and hyper inverse Wishart distributions.
  
We denote our proposed prior as $HIW_c(\delta,W,G)$ and specify
%
\begin{align}
S_k \mid G \sim HIW_c(\delta_k,W_k,G) \quad k=0,\dots,T-1.
\end{align}
%
In the Supplement, we show that this prior specification is \emph{conjugate} to the TGM-based Whittle likelihood of Eq.~\eqref{eq:whittlegraph}. Also note that the graph, $G$, is shared across all frequencies.

\subsection{Marginal likelihood} \label{marglik}
Due to conjugacy of our proposed hyper complex inverse Wishart prior, the marginal likelihood of the time series $\BF{X}_{1:N}$ given a decomposable graph $G$, integrating out the spectral density matrices $S_{0:T-1}$, has a closed form which is derived in the Supplement and given by 
%
\begin{equation}\label{eq:marg}
p(\BF{X}_{1:N}|G) \approx \pi^{-N T p} \prod_{k = 0}^{T - 1}  \frac{h(W_k,\delta_k,G)}{h(W^*_k, \delta_k^*,G)}.
\end{equation}
%
Here, $\delta_k^* = \delta_k + N$, $W^*_k = W_k + P_k$, and
\begin{equation}
h(W,\delta,G) = \frac{\prod_{C \in \mathcal{C}} B(W_C,\delta)}{\prod_{S \in \mathcal{S}} B(W_S, \delta)}.
\end{equation}
%
From the definition of $\delta_k^*$, we see that $N$, the number of time series, acts as the effective number of observations in this case.  For the i.i.d. GGM, $N$ represents the number of independent vector-valued observations; in our TGM, $N$ plays the same role, but represents the number of independent \emph{time series} observations.  Likewise, as in standard inverse Wishart based modeling of covariances for i.i.d. Gaussian data, based on a set of $N$ i.i.d. complex normal observations of Fourier coefficients $d_{nk}$ with covariance $S_k$ (see Eq.~\eqref{eq:whittle}), we update the prior scale matrix $W_k$ with the outer product $P_k = \sum_{n=1}^N d_{nk}d_{nk}^*$, which is the aggregate \emph{periodogram} (see Eq.~\eqref{eq:periodogram}).

Having an analytic marginal likelihood of the time series given a PGM allows us to perform inference directly over graphs, sidestepping any thorny issues with inference directly on the $T$ $p \times p$ spectral density matrices themselves.  This is a critical feature of the practicality of our approach.
 
\subsection{Fractional priors for model selection}
Marginal likelihoods used for model comparison \cite{Kass:1995} are notoriously sensitive to the choice of prior parameters, or \emph{hyperparameters}. In our case, the marginal likelihood in Eq.~\eqref{eq:marg} depends strongly on the hyper complex inverse Wishart scale matrix, $W_k$. Since the scale and shape of the spectral density matrices are not known a priori, and vary dramatically across frequencies, we employ \emph{fractional priors} \cite{Ohagan:1995} over each $S_k$. Fractional priors effectively hold out some fraction of the data, and utilize that fraction to determine an adequate hyperparameter setting for each model. The rest of the data are then used for model comparison. Fractional priors have been deployed for graphical model selection in i.i.d. graphs and have a number of desirable properties such as information consistency and demonstrated robustness \cite{Scott:2008}. In our case, under a fractional prior with parameter $g \in (0,1)$, the fractional marginal likelihood is
%
\begin{equation} \label{eq:fracmarg}
p(\BF{X}_{1:N}|g, G) = \pi^{-N T p} \prod_{k = 0}^{T - 1} \frac{h(g P_k,g N,G)}{h(P_k, N,G)}.
\end{equation}
%
Here, we see that $g$ controls the fraction of data used for prior formulation versus model comparison.  Importantly, we now have just a single, scalar, and interpretable parameter $g$ to tune.  Default settings are suggested in \cite{Ohagan:1995,Scott:2008}.

\subsection{Graph prior}
There are two common approaches in the literature to specifying a prior distribution on graphs. The first approach places a uniform distribution on the space of all possible graphs \cite{Giudici:1999, Dellaportas:2003, Roverato:2002}. As noted in \cite{Armstrong:2009}, this prior puts high weight on graphs with a medium number of edges and significantly less weight on graphs with small or many edges. In response to this problem, it has been proposed to place a prior directly on the size of the graph and then consider a conditionally uniform prior on all graphs of the same size \cite{Armstrong:2009, Dobra:2004, jones:2005}. We follow this later approach and place a binomial distribution on the number of edges, $k$:
\begin{equation}
p(G) \propto r^k (1 - r)^{m - k}, 
\end{equation}
where $r$ is the prior probability that each of $m = \frac{p(p-1)}{2}$ possible undirected edges $(i,j) \in V \times V$ is included. Since $r$ is unknown, we further place a $\text{Beta}(a,b)$ prior over $r$. Integrating out $r$ gives the marginal prior over graphs
\begin{align} \label{eq:multprior}
p(G) \propto \frac{\beta(a + k, b + m - k)}{\beta(a,b)}
\end{align}
where $\beta(.,.)$ is the beta function. As explored in \cite{Scott:2008}, this is a multiplicity correcting prior \cite{Scott:2006} over graphs with the desirable property of diminishing false positive edge discoveries as extra unconnected nodes are added to the graph.

\section{METHODS FOR SINGLE TIME SERIES} \label{Sec:smoothing}
In some applications of interest one observes only a single multivariate time series, $N = 1$, from which the graph must be inferred. Two challenges arise in this setting: (1) the effective number of observations informing Eq.~\eqref{eq:marg} is just one and (2) the periodogram used in computing $W_k^*$ is noisy regardless of the length of the series, $T$.  The periodogram is a notoriously poor estimator of the spectral density, and when the spectral density itself is of primary interest, a common frequentist method is to smooth the periodogram to obtain a consistent spectral density estimator \cite{Jung:2014, Bach:2004, Dahlhaus:2000}. One could imagine using the smoothed periodogram as a plug-in estimator in Eq.~\eqref{eq:marg}, scaled by the effective degrees of freedom (see the Supplement for more details on this plug in estimator for our formulation).  An alternative variance-reduction technique is the Bartlett method \cite{Bartlett:1948}, that divides the length $T$ series into $M$ shorter series of length $\frac{T}{M}$ and averages the resulting $M$ periodograms, but at the cost of reduced resolution (i.e., number of considered frequencies).  This approach mimics the implicit smoothing that occurs when we compute the periodogram based on $N$ truly independent series each of length $T$, as in Eq.~\eqref{eq:periodogram}.

In contrast to a plug-in estimator, a natural Bayesian approach enforces smoothing across frequencies via a prior distribution over the set of spectral densities \cite{Rosen:2007}. Previous approaches have coupled elements of a Cholesky decomposition of each spectral density matrix across frequencies, however this approach is unsuitable to our case since 1) it does not enforce sparsity in the inverse spectral density and 2) a prior of this form will remove the simple marginal likelihood structure in Eq.~\eqref{eq:marg} that we harness for efficient inference. Motivated by our aims to both share information across frequencies and maintain the form of the marginal, we utilize a piecewise constant prior over spectral densities given a graph, $G$. We partition the interval $[0, 2 \pi]$ into $M$ intervals $w_1 = \left[0, \frac{2 \pi}{M}\right), \ldots, w_j = \left[\frac{2 \pi (j - 1)}{M}, \frac{2 \pi j}{M}\right),\ldots, w_{M}=\left[\frac{2 \pi (M - 1)}{M}, 2 \pi \right]$ and then draw a separate positive definite Hermitian matrix from a $HIW_c$ distribution for each interval:
\begin{align}
\tilde{S}_j &\sim  HIW_{c}(\delta,W_j, G) \quad j=1,\dots,M.
\end{align}
%
Our resulting spectral density is simply
%
\begin{align}
S(\lambda) &= \sum_{j = 1}^{M} {\BF 1}_{\lambda \in w_{j}} \tilde{S}_j \quad \forall \lambda \in [0, 2 \pi].
\end{align} 
Under this prior, the marginal likelihood for the single ($N = 1$) time series becomes
\begin{equation}
p(\BF{X}|G) \approx \pi^{-M p} \prod_{j = 1}^{M} \frac{h(W_j,\delta_j,G)}{h(W^*_j, \delta^*_j,G)}
\end{equation}
where $\delta^*_j = \delta_j + \sum_{k = 0}^{T-1} {\BF 1}_{\lambda_k \in w_{j}} $ and $W^*_j = W_j + \sum_{k = 0}^{T-1} {\BF 1}_{\lambda_k \in w_{j}} P_k$. By setting $M = \lfloor \sqrt{T} \rfloor$, we obtain an asymptotically approximate nonparametric prior distribution over continuous spectral density matrices: for $T$ large enough the prior puts positive support on spectral density matrices arbitrarily close to any continuous spectral density over $[0, 2 \pi]$. Furthermore, under this setting as $T \to \infty$, the number of Fourier frequencies, and thus number of samples $\sum_{k = 0}^{T-1} {\BF 1}_{\lambda_k \in w_{j}}$, within each interval grows as $\sqrt T$. 

\section{The Complex Normal and Complex Inverse Wishart Distributions}

The complex normal distribution is a generalization of the multivariate normal
distribution to the complex domain. Let $Z \in \mathbb{C}^{p}$ be a complex
random variable. $Z$ is distributed as a complex normal distribution,
$\mathcal{N}_c(0, \Sigma)$, with zero mean and complex Hermitian positive definite covariance matrix $\Sigma \in \mathbb{C}^{p \times p}$ if it has density given by
\begin{equation} \label{eq:complexnormal}
p(z) = \frac{1}{\pi^p |\Sigma|} e^{-z^* \Sigma^{-1} z},
\end{equation}
where $z^{*} = \bar{z}^T$ denotes the conjugate transpose of $z$. If $Z \sim \mathcal{N}_c(0, \Sigma)$ then the distribution over $Z$ can be represented equivalently as a joint distribution over the real and imaginary elements of $Z = X + i Y$, $X, Y \in \mathbb{R}^{p}$
\begin{equation}
\left[\begin{array}{c}
X \\
Y
\end{array} \right]  \sim N(0, \left[\begin{array}{cc}
\Real \Sigma& - \Imag \Sigma\\
\Imag \Sigma & \Real \Sigma \\
\end{array} \right] ,
\end{equation}
where $\Real \Sigma$ and $\Imag \Sigma$ indicate the real and imaginary
components of $\Sigma$, respectively. Thus we see that the real and imaginary
components are independent iff $\Imag \Sigma = 0$.  As in the non-complex case,
the marginal likelihood of $X_A$ for some subset of nodes $A \subseteq
\{1,\ldots,p\}$, is given by $X_A \sim \mathcal{N}_c(0,\Sigma_{A})$, where $\Sigma_A$ is the matrix formed by selecting the rows and columns of $\Sigma$ in $A$. 

%The sampling distribution of the sums of outer products of the complex normal distribution, $W = \sum_{i = 1}^{n} z_i z_i^*$,  is given by the complex Wishart distribution with density
%\begin{align}
%p(w) = \frac{|\Sigma^{-1}|^n}{\pi^{\frac{k(k - 1)}{2}} \prod_{j = 1}^{p} (n - j)!} |w|^{n - k} e^{ \mbox{tr}\Sigma^{-1} w}
%\end{align} 

The conjugate prior distribution for $\Sigma$ is given by the complex inverse
Wishart, $\Sigma \sim IW_c(\delta,W)$, with degrees of freedom parameter
$\delta > 0$ and centering matrix $W \in \mathbb{C}^{p \times p} $, Hermitian positive definite. Its density is given by
\begin{equation}
p(\Sigma|W,\delta) = B(W,\delta) |\Sigma|^{-(\delta + 2 p)} e^{- \text{tr} W \Sigma^{-1}}
 \end{equation}
with normalization constant
\begin{equation*}
    B(W,\delta) = \frac{|W|^{\delta + p}}{\pi^{\frac{p(p - 1)}{2}} \prod_{j
    = 1}^{p} (\delta + p - j)!}.
\end{equation*}
Note that we have used an alternative parameterization of the inverse Wishart
distribution commonly used in the graphical modeling literature
\cite{Giudici:1999}. The marginal distribution of $\Sigma_A$ where $A \subseteq
\{1,\ldots,p\}$ is given by $\Sigma_{A} \sim IW_c(\delta,W_{A})$.


\section{Marginal Likelihood for the Hyper Complex Inverse Wishart}

We define the hyper-complex inverse Wishart distribution for a graph $G = \{V,E\}$ in the main paper as the restriction of the complex inverse Wishart distribution to $\Sigma \in \mathbb{C}^{p \times p}$ with a zero pattern in $\Sigma^{-1}$ specified by $G$. Its density is given by:
\begin{align} \label{eq:hyper}
p(\Sigma|\delta,W,G ) = {\BF 1}_{\Sigma \in M^{+}(G)} h(W,\delta,G)  |\Sigma|^{-(\delta + 2 p)} e^{- \text{tr} W \Sigma^{-1}}
\end{align}
where $h(W,\delta,G)$ is a normalization constant and
$M^{+}(G)$ is the set of positive definite matrices with zeros in their inverse that obey the conditional independence properties of $G$.

Due to the fact that the complex inverse Wishart distribution is conjugate to
the complex normal distribution for an unrestricted $\Sigma$, by Proposition
5.1 in \cite{Dawid:1993} it follows that the hyper complex inverse Wishart
distribution is a strong hyper-Markov distribution. It follows that for decomposable $G$ the complex hyper inverse Wishart density can be written in terms of the cliques, $\mathcal{C}$, and separators, $\mathcal{S}$, of $G$:
\begin{equation}
p(\Sigma|\delta,W,G ) =  {\BF 1}_{\Sigma \in M^{+}(G)} \frac{\prod_{C \in \mathcal{C}} p(\Sigma_C|W_C,\delta)}{\prod_{S \in \mathcal{S}} p(\Sigma_S|W_S,\delta)}
\end{equation}
where $p(\Sigma_C|W_C,\delta)$ is the unrestricted complex inverse Wishart density for $\Sigma_C$.  This decomposition implies that the normalization constant for Equation \eqref{eq:hyper} is also given by the ratio of complex inverse Wishart normalization constants for cliques and separators
\begin{equation}
h(W,\delta,G) = \frac{\prod_{C \in \mathcal{C}} B(W_C, \delta)}{\prod_{S \in \mathcal{S}} B(W_S,\delta)}.
\end{equation}
If $Z_1,...Z_N \stackrel{i.i.d.}{\sim} \mathcal{N}_c(0,\Sigma)$, then the joint
distribution of $Z_1,\ldots, Z_N$, and $\Sigma$ can be written as:
\begin{align}
p(z_1,\ldots,z_N, \Sigma|G, W, \delta) &\propto \\
 {\BF 1}_{\Sigma \in M^{+}(G)} \frac{h(W,\delta,G)}{\pi^{N p}}  |\Sigma|^{ -(\delta + N + 2p)}   & e^{- \text{tr} (W + \sum_{i = 1}^{N} z_i z_i^*) \Sigma^{-1}} .
\end{align}
We note that the part dependent on $\Sigma$ is the kernel for a $HIW_c(W +
\sum_{i = 1}^{N} z_i z_i^*, \delta + N, G)$ distribution, it follows that the
marginal distribution of $Z_1,\ldots,Z_n|G, W, \delta$ is given by the ratio of
prior and posterior normalization constants of the complex hyper inverse
Wishart distribution times a likelihood constant:
\begin{equation}
p(z_1, \ldots, z_n|G, W, \delta) = \frac{h(W,\delta,G)}{\pi^{Np} h(W + \sum_{i = 1}^{N} z_i z_i^*,\delta + N,G)}.
\end{equation}

\subsection{Marginal Whittle Likelihood}

The model in the main text places independent $HIW_c(W_k, \delta_k, G)$ priors
on each spectral density matrix in the Whittle likelihood, $S_k \sim HIW_c(W_k,
\delta_k, G)$  $\forall k \in [T - 1]$.  Applying the above marginal likelihood
result to each frequency component in the Whittle approximation shows that the
marginal likelihood of the data given a graph, a set of centering matrices
,$W_0,\ldots, W_{T - 1}$, and degrees of freedom, $\delta_0, \ldots
\delta_{T-1}$, for each frequency can be approximated by a product of the normalization constants across frequencies:
\begin{equation}\label{eq:marg}
p(\BF{X}_{1:N}|G) \approx \pi^{-N T p} \prod_{k = 0}^{T - 1}  \frac{h(W_k,\delta_k,G)}{h(W^*_k, \delta^*_k,G)}.
\end{equation}
where $W^*_k = W_k + P_k$ and $\delta^*_k = \delta_k + N$. Indeed, this derivation shows that our prior specification for spectral density matrices is conjugate to the entire Whittle likelihood. 




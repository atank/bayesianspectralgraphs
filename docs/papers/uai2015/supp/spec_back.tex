\section{Spectral Analysis Background}

Spectral analysis is an approach to
analyzing stationary time series where the main object of interest is the
\emph{spectral density}. For many applications, the spectral density is often a
more informative object about the underlying physical process than
autoregressive coefficients or the lagged autocovariance. In particular, it
allows one to read off which frequency components are prominent in a time
series, and usually certain frequency bands have scientific relevance. The
theoretical justification for caring about the spectral density arises from the
classic \emph{spectral representation theorem} \cite{Brockwell:1991}.
Informally, this theorem states
that under some conditions, any stationary vector stochastic process $x(t) \in
\mathbb{R}^{p}$ can be written as
\begin{equation}
x(t) = \int_{-\pi}^{\pi} e^{-i t \lambda} dZ(\lambda)
\end{equation}
where $d Z(\lambda) \in \mathbb{C}^{p}$ is an \emph{orthogonal increments
process} such that $E (Z(\lambda) Z(\lambda)^*) = S(\lambda)$, where
$S(\lambda)$ is the spectral density matrix and $E (Z(\lambda) Z(\lambda')^*) =
0$ for $\lambda \neq \lambda'$. Intuitively, this theorem states that the
amplitudes of certain frequencies in a stationary process are independent
across frequencies and the within frequency covariance is given by the spectral density matrix. As noted in the main text, the spectral density matrix also arises as the Fourier transformation of the lagged auto covariance matrices of the process
\begin{equation}
S(\lambda) = \sum_{h = - \infty}^{\infty} \Gamma(h) e^{-i \lambda h}
\end{equation}
where $\Gamma(h) = E(x(t) x(t + h)^T)$. The statistical task then becomes estimation of the spectral density matrix $S(\lambda)$ from a finite observed time series $x = [x(1),...,x(T)]$. The most basic estimator is given by the periodogram, defined as:
\begin{equation}
P_k = d_k d_k^*
\end{equation}
where $d_k$ is the DFT of the observed time series at Fourier frequency
$\lambda_k$, (Eq. (7) of the main text). While this estimator is asymptotically
unbiased, it is not consistent since its variance does not go to zero. Instead,
techniques that smooth across nearby frequencies provide consistent estimators
of the spectral density, and are commonly used in practice \cite{Percival:1993}.  Finally, the
periodogram estimates at different frequencies, $P_k$ and $P_k'$, are
asymptotically uncorrelated, providing some intuition as to why the Whittle
approximation decomposes into independent terms for each frequency~\cite{Brillinger:2001}. More details on the spectral approach to analyzing stationary time series is provided in Brillinger, 2001~\cite{Brillinger:2001}.


\section{Smoothing the Periodogram for a Single Time Series via the Plug in Method} \label{Sec:smoothing}

In this section we provide more details on the plug in method for smoothing the
periodogram obtained from a single realization of a multivariate time series
mentioned in Sec. 4 of the main text. First we provide some background on
classical frequentist approaches to smoothing the periodogram to obtain
consistent estimators \ of the spectral density (as $T$ increases).

When the spectral density itself is the primary object of interest, a common frequentist method is to smooth the periodogram to obtain a consistent estimator of the spectral density:
\begin{equation}
\hat{S}(\lambda_k) = \sum_{|j| < m} W_T(j) P_{k + j}
\end{equation}
where  $P_{k}$ is the periodogram at frequency $\lambda_k$ as introduced in the
main text and $W_T(j) \geq 0$, $\sum_{|j| < m} W_T(j) = 1$ are some smoothing
weights for a length $T$ series and $m$ is the smoothing window. This approach
was used in the frequentist graph estimation frameworks in \cite{Jung:2014,
Bach:2004, Dahlhaus:2000}. To ensure consistency as $T \to \infty$ we must have
$m \to \infty$, $\frac{m}{n} \to 0$,   and $\sum_{|j| < m} W_T(j)^2 \to 0$
\cite{Brockwell:1991}. The asymptotic variance of $\hat{S}_k$ scales as
$\sum_{|j| \leq m} W_T^2(j)$, implying that the asymptotic effective sample
size for a smoothed estimate of this form is $(\sum_{|j| \leq m}
W_T^2(j))^{-1}$ \cite{Brockwell:1991}. The Daniell smoother corresponds to
taking $W_T(j) = \frac{1}{2m+1}$ and has an intuitive (effective) sample size of $2m + 1$, the size of the smoothing window. Intuitively, this holds asymptotically since as $T \to \infty$ the sample periodograms become independent at different frequencies implying a sample size of $2m + 1$, the number of (asymptotically) independent samples. 

Inspired by the use of this smoothing technique in previous TGM procedures
$\cite{Jung:2014, Bach:2004, Dahlhaus:2000}$ we develop a similar procedure
tailored to our objective function in Eq. \eqref{eq:marg}. We plug in a
smoothed estimate of the spectral density matrix, scaled by the asymptotic
effective degrees of freedom, for the priodogram, $P_k$, in Eq. \eqref{eq:marg}. Specifically, we set $W^*_k = W_k + (\sum_{|j| \leq m} W_T^2(j))^{-1} \hat{S}_k$.  The degrees of freedom parameter $\delta^*_k$ is similarly updated by adding the effective sample size of the smoother to the prior degrees of freedom: $\delta^*_k = \delta_k +  (\sum_{|j| \leq m} W_T^2(j))^{-1}$. If we use the Daniell smoother outlined above the updates become $W^*_k = W_k + \sum_{|j| \leq m} P_{k + j}$ and $\delta^* = \delta_k + 2m + 1$. In practice we set $m = \lfloor \frac{\sqrt{T}}{2} \rfloor$ to ensure that the conditions for consistency of $\hat{S}_k$ are met.
